import csv
import datetime
import json
import os
import threading
from os.path import isfile, join
from queue import Queue
import time

import re

import pandas as pd
from flask import Flask

from easy_order.MongoDB_Utilities import MongoDB_utilities
from models import ContactType
from models.ContactType import ContactyType
from models.GeoJSON import geo_json
from utilities import geography_utilities, utilities
from utilities.Scraper import Scraper
from utilities.utilities import print_and_emit

#for flask_socketio
EVENT='message'
NAMESPACE='/'

class Update_companies_in_DB:
    def __init__(self, mongo_server,mongo_port,mongodb):
        self.mongo_server=mongo_server
        self.mongo_port=mongo_port
        self.mongodb=mongodb

    def reindex(self,db_collection_name):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        mdb.reindex_collection(db_collection_name)

    def create_index_for_vat(self, collection_name):
        mdb=MongoDB_utilities(self.mongo_server,self.mongo_port,self.mongodb)
        mdb.create_index_for_vat(collection_name)


    def add_full_address_from_kbo_csv_file(self,db_collection_name, input_csv_file,check_if_record_exist:bool,sep,threads:int):
        url_queue = Queue()

        def process_queue():
            while True:
                vat, address_data, current_row = url_queue.get()
                mdb.insert_item_with_vat_as_key(db_collection_name,address_data,True)
                print('added no. {} of {} | vat {} | data: {}'.format(current_row,row_count,address_data["vat"],address_data))
                url_queue.task_done()

        for i in range(threads):
            t = threading.Thread(target=process_queue)
            t.daemon = True
            t.start()

        start = time.time()

        mdb=MongoDB_utilities(self.mongo_server,self.mongo_port,self.mongodb)
        current_row = 0
        row_count = sum(1 for line in open(input_csv_file,encoding="utf-8" ))

        with open(input_csv_file, 'r', encoding='utf-8') as csvfile:
            csvreader = csv.reader(csvfile,delimiter=sep)
            for row in csvreader:
                current_row+=1
                address_data = {}
                if current_row == 1:
                    continue  # the first row is the row with headers
                vat = row[0]

                address_data["street"]=row[7]
                if len(address_data["street"])==0:
                    address_data["street"]=row[8]
                address_data["houseno"]=row[9]
                address_data["box"]=row[10]
                address_data["zipcode"]=row[4]
                address_data["city"]=row[5]
                if len(address_data["city"])==0:
                    address_data["city"]=row[6]
                address_data["country"]=row[2]
                if len(address_data["country"])==0:
                    address_data["country"]=row[3]
                    if len(address_data["country"])==0:
                        address_data["country"]='Belgium'
                #clean data:
                address_data["vat"]='BE'+vat.strip(' \n').replace('\r', '').replace('.','')
                address_data["street"]=address_data["street"].strip(' \n').replace('\r', '')
                address_data["city"]=address_data["city"].strip(' \n').replace('\r', '')
                address_data["country"]=address_data["country"].strip(' \n').replace('\r', '')
                # if check_if_record_exist and mdb.find_company(address_data["vat"], db_collection_name) is not None:
                #     print('not added since existing already no. {} of {} | vat {} | data: {} '.format(current_row,
                #                                                                                        row_count,address_data[
                #                                                                                            "vat"],address_data))
                #     continue
                url_queue.put((vat, address_data, current_row))

        print(threading.enumerate())

        print("Execution time = {0:.5f}".format(time.time() - start))
        return None


    def add_contact_from_kbo_csv_file(self,db_collection_name, input_csv_file,sep):
        start = time.time()

        mdb=MongoDB_utilities(self.mongo_server,self.mongo_port,self.mongodb)
        current_row = 0
        row_count = sum(1 for line in open(input_csv_file,encoding="utf-8" ))

        with open(input_csv_file, 'r', encoding='utf-8') as csvfile:
            csvreader = csv.reader(csvfile,delimiter=sep)
            for row in csvreader:
                current_row+=1
                contact_data = {}
                if current_row == 1:
                    continue  # the first row is the row with headers
                vat = row[0]

                contact_data["EntityContact"]=row[1]
                contact_data["ContactType"]=row[2]
                contact_data["Value"]=row[3]

                #clean data:
                contact_data["vat"]='BE'+vat.strip(' \n').replace('\r', '').replace('.','')
                contact_data["EntityContact"]=contact_data["EntityContact"].strip(' \n').replace('\r', '')
                contact_data["Value"]=contact_data["Value"].strip(' \n').replace('\r', '')

                mdb.insert_item_with_vat_as_key(db_collection_name, contact_data, True)
                print('added {} of {}'.format(current_row,row_count))

        print(threading.enumerate())

        print("Execution time = {0:.5f}".format(time.time() - start))
        return None


    def add_company_names_from_kbo_csv_file(self,db_collection_name, input_csv_file,sep):
        start = time.time()

        mdb=MongoDB_utilities(self.mongo_server,self.mongo_port,self.mongodb)
        current_row = 0
        row_count = sum(1 for line in open(input_csv_file,encoding="utf-8" ))

        with open(input_csv_file, 'r', encoding='utf-8') as csvfile:
            csvreader = csv.reader(csvfile,delimiter=sep)
            for row in csvreader:
                current_row+=1
                company_name_data = {}
                if current_row == 1:
                    continue  # the first row is the row with headers
                vat = row[0]
                company_name_data["Language"]=row[1]
                company_name_data["TypeOfDenomination"]=row[2]
                company_name_data["Denomination"]=row[3]
                try:
                    company_name_data["Denomination_part2"] = row[4].strip(' \n').replace('\r', '')
                except:
                    pass

                #clean data:
                company_name_data["vat"]='BE'+vat.strip(' \n').replace('\r', '').replace('.','')
                company_name_data["Language"]=company_name_data["Language"].strip(' \n').replace('\r', '')
                company_name_data["TypeOfDenomination"]=company_name_data["TypeOfDenomination"].strip(' \n').replace('\r', '')
                company_name_data["Denomination"]=company_name_data["Denomination"].strip(' \n').replace('\r', '')


                mdb.insert_item_with_vat_as_key(db_collection_name, company_name_data, True)
                print('added {} of {}'.format(current_row,row_count))

        print(threading.enumerate())

        print("Execution time = {0:.5f}".format(time.time() - start))
        return None

    def collect_all_email_address(self,output_full_path,db_collection,optional_vat_array, sep=';'):
        email_address_regx=r'[\w\.-]+@[\w\.-]+\.\w+'
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        line_to_append='url'+sep+'email_address'+sep
        dict = {}
        if optional_vat_array == None:
            cursor = mdb.find_scraped_content(keyword=email_address_regx,
                                                   mongodb_collection_scraped_content=db_collection,
                                                   timeout=True)
        else:
            cursor = mdb.find_scraped_content_with_given_vat_array(keyword=email_address_regx,
                                                                        mongodb_collection_scraped_content=db_collection,
                                                                        vat_array=optional_vat_array, timeout=True)
        total_records_with_mail_address=cursor.count()
        current_record=0
        for document in cursor:
            current_record+=1
            print('thread {} | Working on record {} of {} | starting to check for e-mail addresses'.format(threading.get_ident(),current_record,total_records_with_mail_address))
            url_not_parsed=document['url_not_parsed']
            all_email_addresses_list = re.findall(email_address_regx, document['scrapedContent'])
            all_email_addresses_set=set()
            for email_address in all_email_addresses_list:
                #for example this info@restokopenhagen.beOPENINGSUREN needs to be fixed to info@restokopenhagen.be
                for suffix in ['.be','.com']:
                    suffix=suffix.lower()
                    if suffix in email_address.lower(): #does the email_address containt suffix?
                        email_address = email_address.rpartition(suffix)[0] + email_address.rpartition(suffix)[1]
                        all_email_addresses_set.add(email_address)
                        continue
                    # regex to use: "(.* ?)\.com"
                    # if re.match("(.* ?)\.{}".format(suffix),email_address) is not None:
                    #     email_address=re.match("(.*?){}".format(suffix),email_address).group()


            sum_of_email_addresses_per_record=len(all_email_addresses_list)
            current_mail_address_no=0
            for email_address in all_email_addresses_set:
                current_mail_address_no+=1
                print('Thread {} | Working on record {} of {} | e-mail {} of {} for current record'
                      .format(threading.get_ident(),current_record,total_records_with_mail_address,
                      current_mail_address_no,sum_of_email_addresses_per_record))
                line_to_append+='\n'+url_not_parsed + sep + email_address+sep
                utilities.append_text_to_text_file(fullpath=output_full_path, string_to_append=line_to_append, mode='a')
                line_to_append=''  #reset line for next round


        cursor.close()

    def add_employees_recorded(self, annual_accounts_input_csv_file,check_if_record_exist,collection_name_recorded_employees ):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        mdb.create_index_for_vat(collection_name_recorded_employees)
        number_of_processed =0
        current_row=0
        row_count = sum(1 for line in open(annual_accounts_input_csv_file,encoding="utf-8" ))
        with open(annual_accounts_input_csv_file, 'r',encoding='utf-8') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=',')
            for row in csvreader:
                current_row += 1
                if current_row == 1:
                    continue  # the first and second row is a row with headers

                employeesRecorded = row[62]
                vat = row[1]
                recorded_employees_data = {}
                recorded_employees_data["vat"] = vat
                recorded_employees_data["recorded_employees"] = employeesRecorded
                if len(employeesRecorded)==0:
                    print('not added since no employeesRecorded found  no. {} of {} | vat {} | data: {} '.format(current_row,row_count,vat,recorded_employees_data))
                    continue

                # if check_if_record_exist and mdb.find_company(vat, collection_name_recorded_employees) is not None:
                #     print('not added since existing already no. {} of {} | vat {} | data: {} '.format(current_row,row_count,
                #                                                                                        vat,recorded_employees_data))
                #     continue
                result=None
                while result is None:
                    result=mdb.insert_item_with_vat_as_key(collection_name_recorded_employees,recorded_employees_data, True)
                print(
                    'added no. {} of {} | vat {} | data: {} '.format(current_row, row_count,
                                                                                                vat,
                                                                                                recorded_employees_data))

                #break
            print('done')

    def find_company_within_coords_distance(self,db_latlong_collection,lat:float, long:float, min_distance_meters:float):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        result = mdb.find_company_with_given_coords(db_latlong_collection,lat,long,min_distance_meters)
        # result=db[db_collection].find({"name":"Imelda-Instituut"})
        # regx = re.compile("Imelda-Instituut.*", re.IGNORECASE)
        # result=db[db_collection].find({"name": regx})
        # for document in db[db_collection].find({}, {'_id': False}):
        #     print(document)
        for company_item in result:
            print(company_item)
            break

    def collect_btws_from_given_list_and_add_coords_to_db(self,app:Flask,vat_array, vat_column_id, db_full_address_collection,
                                                          db_coords_collection):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        current_item_count = 0
        row_count = len(vat_array)
        for vat in vat_array:
                if len(vat) == 0:
                    print(
                        'skipping since no vat found | no. {} of {}'.format(current_item_count, row_count))
                    continue
                vat = re.sub("(?i)be", "BE", vat.strip())  # replace be to BE (case insensitive)
                if not vat.startswith('BE'):
                    vat = 'BE' + vat.replace('.', '')

                if mdb.find_company(vat, db_coords_collection) is not None:
                    print(
                        'skipping since latlong already found for vat {}| no. {} of {}'.format(vat, current_item_count,
                                                                                               row_count))
                    if app is not None:
                        print_and_emit(app,'skipping since latlong already found for vat {}| no. {} of {}'.format(vat, current_item_count,row_count),EVENT,NAMESPACE)
                    continue
                document = mdb.find_company(vat, db_full_address_collection)
                if document is None:
                    print(
                        'skipping since no address found for vat {}| no. {} of {}'.format(vat, current_item_count,
                                                                                          row_count))
                    continue
                print(document)
                street = document["street"]
                houseno = document["houseno"]
                postcode = document["zipcode"]
                city = document["city"]
                address = str(street + ' ' + str(houseno) + ' ,' + str(postcode) + ' ' + city)
                lat_long_string = geography_utilities.get_lat_long_all_methods(address)
                if lat_long_string is not None:
                    lat = float(lat_long_string.split(',')[0])
                    long = float(lat_long_string.split(',')[1])
                else:
                    print(
                        'not added since no coords found | no. {} of {} | vat {} | data: None added'.format(
                            current_item_count,
                            row_count,
                            vat))
                    continue
                new_lat_long_data = {}
                new_lat_long_data["vat"] = vat
                new_lat_long_data["location"] = geo_json("Point", lat=lat, long=long).to_dict()
                mdb.insert_item_with_vat_as_key(db_coords_collection, new_lat_long_data, True)
                print(
                    'added | no. {} of {} | vat {} | data: {} '.format(current_item_count, row_count,
                                                                       vat,
                                                                       new_lat_long_data))
                if app is not None:
                    print_and_emit(app,'added | no. {} of {} | vat {} | data: {} '.format(current_item_count, row_count,
                                                                       vat,
                                                                       new_lat_long_data),EVENT,NAMESPACE)

    def add_latlong_with_threads(self,db_latlong_collection, db_companies_collection,threads:int):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        name_location_column="location"
        #mdb.create_index_for_coords(db_latlong_collection,name_location_column=name_location_column)

        latlong_queue = Queue()
        def process_queue():
            while True:
                current_item_count, row_count, document = latlong_queue.get()

                vat = document["vat"]
                if mdb.find_company(vat, db_latlong_collection) is not None:
                    print(
                        'Thread {} | latlong not added since already added | no. {} of {} | vat {} | data: None added'.format(
                            threading.get_ident(), current_item_count,
                            row_count,
                            vat))
                    continue
                street = document["street"]
                houseno = document["houseno"]
                postcode = document["zipcode"]
                city = document["city"]
                address = str(street + ' ' + str(houseno) + ' ,' + str(postcode) + ' ' + city)
                lat_long_string = geography_utilities.get_lat_long_all_methods(address)
                if lat_long_string is not None:
                    lat = float(lat_long_string.split(',')[0])
                    long = float(lat_long_string.split(',')[1])
                else:
                    print(
                        'Thread {} | latlong not added since no coords found | no. {} of {} | vat {} | data: None added'.format(
                            threading.get_ident(), current_item_count,
                            row_count,
                            vat))
                    continue
                new_lat_long_data = {}
                new_lat_long_data["vat"] = vat
                new_lat_long_data[name_location_column] = geo_json("Point", lat=lat, long=long).to_dict()
                mdb.insert_item_with_vat_as_key(db_latlong_collection, new_lat_long_data, True)
                print(
                    'Thread {} | latlong added | no. {} of {} | vat {} | data: {} '.format(threading.get_ident(),current_item_count, row_count,
                                                                       vat,
                                                                       new_lat_long_data))

                latlong_queue.task_done()

        for i in range(threads):
            t = threading.Thread(target=process_queue)
            t.daemon = True
            t.start()

        start = time.time()


        current_item_count=0
        cursor=mdb.get_all_companies(db_companies_collection,timeout=True)
        cursor.skip(193859) #TODO: verwijder eventueel
        row_count = cursor.count()
        for document in cursor:
            current_item_count += 1
            latlong_queue.put((current_item_count, row_count,document))
        cursor.close()
        latlong_queue.join()

        print(threading.enumerate())

        print("Execution time = {0:.5f}".format(time.time() - start))

            #break


    def add_latlong(self,db_latlong_collection, db_full_address_collection,number_to_skip_of_first_results:int):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        name_location_column="location"
        current_item_count=0
        cursor=mdb.get_all_companies(db_full_address_collection,timeout=True)
        row_count = cursor.count()
        cursor.skip(number_to_skip_of_first_results)
        current_item_count+=number_to_skip_of_first_results
        #mdb.create_index_for_coords(db_latlong_collection,name_location_column=name_location_column)
        for document in cursor:
            current_item_count += 1
            vat = document["vat"]
            if mdb.find_company(vat, db_latlong_collection) is not None:
                print(
                    'latlong not added since already added | no. {} of {} | vat {} | data: None added'.format(current_item_count,
                                                                                                 row_count,
                                                                                                 vat))
                continue
            street = document["street"]
            houseno=document["houseno"]
            postcode=document["zipcode"]
            city=document["city"]
            address = str(street + ' ' + str(houseno) + ' ,' + str(postcode) + ' ' + city)
            lat_long_string = geography_utilities.get_lat_long_all_methods(address)
            if lat_long_string is not None:
                lat = float(lat_long_string.split(',')[0])
                long = float(lat_long_string.split(',')[1])
            else:
                print(
                    'latlong not added since no coords found | no. {} of {} | vat {} | data: None added'.format(current_item_count,
                                                                                                row_count,
                                                                                                vat))
                continue
            new_lat_long_data = {}
            new_lat_long_data["vat"] = vat
            new_lat_long_data[name_location_column]=geo_json("Point",lat=lat,long=long).to_dict()
            mdb.insert_item_with_vat_as_key(db_latlong_collection, new_lat_long_data, True)
            print(
                'latlong added | no. {} of {} | vat {} | data: {} '.format(current_item_count, row_count,
                                                                   vat,
                                                                   new_lat_long_data))
            #break
        cursor.close()

    def add_latlong_with_other_then_companies_db(self,db_latlong_collection, db_full_address_collection,db_not_a_full_address_collection,number_to_skip_of_first_results:int):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        name_location_column = "location"
        current_item_count = 0
        cursor = mdb.get_all_companies(db_not_a_full_address_collection, timeout=True)
        cursor.skip(number_to_skip_of_first_results)
        current_item_count+=number_to_skip_of_first_results
        row_count = cursor.count()
        # mdb.create_index_for_coords(db_latlong_collection,name_location_column=name_location_column)
        for document in cursor:
            current_item_count += 1
            vat = document["vat"]
            if mdb.find_company(vat, db_latlong_collection) is not None:
                print(
                    'latlong not added since already added | no. {} of {} | vat {} | data: None added'.format(
                        current_item_count,
                        row_count,
                        vat))
                continue
            full_address_document=mdb.find_company(vat,db_full_address_collection)
            if full_address_document==None:
                print(
                    'latlong not added since no address found in fulladdress collection | no. {} of {} | vat {} | data: None added'.format(
                        current_item_count,
                        row_count,
                        vat))
                continue
            street = full_address_document["street"]
            houseno = full_address_document["houseno"]
            postcode = full_address_document["zipcode"]
            city = full_address_document["city"]
            address = str(street + ' ' + str(houseno) + ' ,' + str(postcode) + ' ' + city)
            lat_long_string = geography_utilities.get_lat_long_all_methods(address)
            if lat_long_string is not None:
                lat = float(lat_long_string.split(',')[0])
                long = float(lat_long_string.split(',')[1])
            else:
                print(
                    'latlong not added since no coords found | no. {} of {} | vat {} | data: None added'.format(
                        current_item_count,
                        row_count,
                        vat))
                continue
            new_lat_long_data = {}
            new_lat_long_data["vat"] = vat
            new_lat_long_data[name_location_column] = geo_json("Point", lat=lat, long=long).to_dict()
            mdb.insert_item_with_vat_as_key(db_latlong_collection, new_lat_long_data, True)
            print(
                'latlong added | no. {} of {} | vat {} | data: {} '.format(current_item_count, row_count,
                                                                           vat,
                                                                           new_lat_long_data))
            # break
        cursor.close()


    def delete_documents_with_empty_field(self,db_collection,field_name):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        mdb.delete_documents_with_empty_field(db_collection,field_name=field_name)

    def delete_documents_with_given_array(self,db_collection,vat_array):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        for vat in vat_array:
            mdb.delete_document_with_given_vat(db_collection,vat)

    def add_url_from_bsearch_with_jsons_as_input(self,db_urls_collection, inputdir_with_bsearch_json):
        number_of_processed=0
        number_of_input_files = len([f for f in os.listdir(inputdir_with_bsearch_json) if isfile(join(inputdir_with_bsearch_json, f))])
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        mdb.create_index_for_vat(db_urls_collection)
        for file in [f for f in os.listdir(inputdir_with_bsearch_json) if isfile(join(inputdir_with_bsearch_json, f))]:
            number_of_processed += 1
            # print('Thread {} | JsonFile: {} | Processing {} from {}'.format(threading.get_ident(), file,
            #                                                                 number_of_processed,
            #                                                                 number_of_input_files))

            with open(inputdir_with_bsearch_json + file, encoding='utf-8', errors='ignore') as company_json_file:
                counter = 0
                # print ('json_file: '+json_file)
                with open(company_json_file.name, 'r') as f:
                    for line in f:

                        # load valid lines (should probably use rstrip)
                        # if len(line) < 10: continue
                        try:
                            # print(collection)
                            new_data = {}
                            old_data = json.loads(line)
                            vat=old_data["vat"]
                            if len(vat) == 0:
                                print(
                                    'skipping since no vat found | no. {} of {}'.format(number_of_processed, number_of_input_files))
                                continue
                            new_data["vat"] = old_data["vat"]
                            new_data["name"] = old_data["name"]
                            new_data["address"] = str(old_data["address"]).replace('\n ',' ').replace('\r\n', '').strip()
                            new_data["phone"] = old_data["phone"]
                            url = old_data["url"]
                            if len(url)>0:
                                new_data["url"] = url
                                print(
                                    'found url | no. {} of {}'.format(number_of_processed,
                                                                                        number_of_input_files))

                            new_data["lastupdated"] = old_data["lastupdated"]

                            # db[db_collection].insert(new_data)
                            result = mdb.insert_item_with_vat_as_key(db_urls_collection,
                                                                     new_data, True)
                            print('Thread {} | added no. {} of {} | vat {} | data: {} '.format(threading.get_ident(),
                                                                                               number_of_processed,
                                                                                               number_of_input_files,
                                                                                               vat,new_data))
                        except ValueError as e:
                            print("Value Error: ", e)
                            # friendly log message
                            if 0 == counter % 100 and 0 != counter:
                                print("loaded line: ", counter)

                    f.close()




    # def add_url_from_bsearch_with_csv_as_input(self, db_urls_collection, bakkerijen_csv_file, vat_column_id:int):
    #     number_of_processed =0
    #     current_row=0
    #     row_count = sum(1 for line in open(bakkerijen_csv_file,encoding="utf-8" ))
    #     mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
    #     mdb.create_index_for_vat(db_urls_collection)
    #     with open(bakkerijen_csv_file, 'r', encoding='utf-8') as csvfile:
    #         csvreader = csv.reader(csvfile, delimiter=';')
    #         for row in csvreader:
    #             current_row += 1
    #             if current_row == 1:
    #                 print(
    #                     'skipping for current_row is headerrow | no. {} of {}'.format(current_row, row_count ))
    #                 continue  # the first and second row is a row with headers
    #
    #             vat = row[0]
    #             if len(vat) == 0:
    #                 print(
    #                     'skipping since no vat found | no. {} of {}'.format(current_row, row_count ))
    #                 continue
    #             vat='BE'+vat.strip(' \n').replace('\r', '').replace('.','')
    #             # if mdb.find_company(vat, db_urls_collection) is not None:
    #             #     print(
    #             #         'not added since already added | no. {} of {} | vat {} | data: None added'.format(
    #             #             current_row,
    #             #             row_count,
    #             #             vat))
    #             #     continue
    #             if mdb.find_company(vat,db_urls_collection) is not None:
    #                 print(
    #                     'skipping since url already found for vat {}| no. {} of {}'.format(vat, current_row, row_count))
    #                 continue
    #             collected_company=scrape_vat(vat)
    #             if collected_company is None:
    #                 print(
    #                     'skipping since no url found for vat {}| no. {} of {}'.format(vat, current_row, row_count))
    #                 continue
    #             url=collected_company.url
    #             if len(url)==0:
    #                 print(
    #                     'skipping since no url found for vat {}| no. {} of {}'.format(vat, current_row, row_count))
    #                 continue
    #             url_data = {}
    #             url_data["vat"] = vat
    #             url_data["url"] = url
    #             url_data["address"] = collected_company.address
    #             url_data["phone"]=collected_company.phone
    #             url_data["name"]=collected_company.name
    #             url_data["lastupdated"]=collected_company.lastupdated
    #
    #             result = None
    #             while result is None:
    #                 result = mdb.insert_item_with_vat_as_key(db_urls_collection,
    #                                                          url_data, True)
    #             print(
    #                 'added no. {} of {} | vat {} | data: {} '.format(current_row, row_count,
    #                                                                  vat,
    #                                                                  url_data))
    #
    #             # break
    #         print('done')

    def add_to_db_from_older_scraped_url_contents(self, collection_companies,db_urls_collection, old_url_files_dir):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        mdb.create_index_for_vat(db_urls_collection)
        number_of_processed =0
        current_row=0
        number_of_input_files = len([f for f in os.listdir(old_url_files_dir) if isfile(join(old_url_files_dir, f))])

        for file in [f for f in os.listdir(old_url_files_dir) if isfile(join(old_url_files_dir, f))]:
            current_row+=1
            with open(old_url_files_dir + file, encoding='utf-8', errors='ignore') as json_file:
                with open(json_file.name, 'r') as f:
                    for line in f:
                        # load valid lines (should probably use rstrip)
                        # if len(line) < 10: continue
                        try:
                            # print(collection)
                            new_data = {}
                            old_data = json.loads(line)
                            new_data["id"] = old_data["id"]
                            new_data["url_not_parsed"]=old_data["url_not_parsed"]
                            new_data["url_parsed"]=old_data["url_parsed"]
                            new_data["scrapedContent"]=old_data["scrapedContent"]
                            vat=mdb.find_company_with_given_url_unparsed(new_data["url_not_parsed"],db_urls_collection)
                            if len(new_data["url_not_parsed"])==0:
                                print(
                                    'skipping since no url | no. {} of {} | vat {} | data: {} '.format(current_row, number_of_input_files,
                                                                                     vat,
                                                                                     new_data))
                                continue
                            if vat == None or len(vat)==0:
                                print(
                                    'skipping since no vat | no. {} of {} | vat {} | data: {} '.format(current_row,
                                                                                        number_of_input_files,
                                                                                        vat,
                                                                                        new_data))
                                continue
                            new_data["vat"]=vat
                            # db[db_collection].insert(new_data)
                            mdb.insert_item_with_vat_as_key(db_urls_collection,new_data,True)
                            print(
                                'added no. {} of {} | vat {} | data: {} '.format(current_row, number_of_input_files,
                                                                                 vat,
                                                                                 new_data))
                        except ValueError as e:
                            print("Value Error: ", e)





    def insert_scraped_url_contents_file_in_db(self,json_file: str, urls_collection,db__scraped_urls_collection, upsert):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        counter = 0
        # print ('json_file: '+json_file)
        with open(json_file, 'r') as f:
            for line in f:

                # load valid lines (should probably use rstrip)
                # if len(line) < 10: continue
                try:
                    # print(collection)
                    new_data = {}
                    old_data = json.loads(line)
                    new_data["vat"]=old_data["vat"]
                    if len(new_data["vat"]) ==0:
                        new_data["vat"]=mdb.find_company_with_given_url_unparsed(old_data["url_not_parsed"],urls_collection)
                    #new_data["id"] = old_data["id"]
                    new_data["url_not_parsed"] = old_data["url_not_parsed"]
                    new_data["url_parsed"] = old_data["url_parsed"]
                    new_data["scrapedContent"] = (' '.join(old_data["scrapedContent"]))
                    new_data["lastupdated"]=old_data["lastupdated"]
                    # db[db_collection].insert(new_data)
                    mdb.insert_item_with_vat_as_key(db__scraped_urls_collection,new_data,True)
                except ValueError as e:
                    print("Value Error: ", e)
                    # friendly log message
                    if 0 == counter % 100 and 0 != counter:
                        print("loaded line: ", counter)

            f.close()

    def add_scraped_url_contents_from_json_files(self,urls_collection,db_scraped_urls_collection, input_dir_with_json_files):
        number_of_input_files = len([f for f in os.listdir(input_dir_with_json_files) if isfile(join(input_dir_with_json_files, f))])
        number_of_processed = 0
        for file in [f for f in os.listdir(input_dir_with_json_files) if isfile(join(input_dir_with_json_files, f))]:
            number_of_processed += 1
            print('Thread {} | JsonFile: {} | Processing {} from {}'.format(threading.get_ident(), file,
                                                                            number_of_processed,
                                                                            number_of_input_files))
            with open(input_dir_with_json_files + file, encoding='utf-8', errors='ignore') as company_json_file:
                self.insert_scraped_url_contents_file_in_db(json_file=company_json_file.name, urls_collection=urls_collection,db__scraped_urls_collection=db_scraped_urls_collection, upsert=True)

            print('Thread {} | JsonFile: {} | Processed {} from {}'.format(threading.get_ident(), file,
                                                                           number_of_processed,
                                                                           number_of_input_files))

    def save_document_contents_in_bear_texts(self,collection,vat_array:list,output_dir,field_list_to_incl_in_bear_text:list):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        number_of_processed = 0
        total_records=len(vat_array)
        for vat in vat_array:
            print('Starting with export no {} of {}'.format(number_of_processed+1,total_records))
            document=mdb.find_company_with_given_vat(vat,collection)
            for field in field_list_to_incl_in_bear_text:
                try:
                    field_content=document[field]
                    if len(field_content)>0:
                        utilities.append_text_to_text_file(output_dir+'{}.txt'.format(vat),field_content+'\n','a')
                    number_of_processed += 1
                except:
                    pass
            print('Finished with export no {} of {}'.format(number_of_processed + 1, total_records))
        print('Finished processing | processed {} with content out of {} records'.format(number_of_processed,total_records))


    def generate_csv_list_from_csv_input_with_output_of_vat_url_emails_phone_etc(self, input_csv, vat_column, source_name_column, urls_collection, scraped_content_collection, contacts_collection, full_addresses_collection, company_names_denomination_collection, output_csv_file, seperator:str):
        current_row=0
        vat_name_array = utilities.get_vat__source_name_array_from_csv_file(input_csv, vat_column,source_name_column)
        row_count=len(vat_name_array)
        scraper = Scraper(None, self.mongo_server, self.mongo_port, self.mongodb)
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        utilities.append_text_to_text_file(output_csv_file, str('vat' + seperator + 'source_name' + seperator + 'name' + seperator + 'url' + seperator + 'email(s)' + seperator + 'phone' + seperator + 'street' + seperator + 'houseno' + seperator + 'box' + seperator + 'city' + seperator + 'zipcode' + seperator + 'country' + seperator), 'w')
        accessed_google_places: int = 0
        for vat,source_name in vat_name_array:

            current_row+=1
            print('Thread {} | Working on row {} of {} rows'.format(threading.get_ident(),current_row,row_count))
            document_from_url_collection = mdb.find_company_with_given_vat(vat, urls_collection)
            # get document_full_address
            document_full_address = mdb.find_company(vat, full_addresses_collection)
            document_company_names=mdb.find_company_name_denomination(vat, company_names_denomination_collection, 3)

            phone = self.get_value_from_document_or_default(document_from_url_collection, 'phone', '')
            street = self.get_value_from_document_or_default(document_full_address, 'street', '')
            houseno = self.get_value_from_document_or_default(document_full_address, 'houseno', '')
            box = self.get_value_from_document_or_default(document_full_address, 'box', '')
            city = self.get_value_from_document_or_default(document_full_address, 'city', '')
            zipcode = self.get_value_from_document_or_default(document_full_address, 'zipcode', '')
            country = self.get_value_from_document_or_default(document_full_address, 'country', '')

            emails_as_string = ''
            email_list=set()

            name = self.get_value_from_document_or_default(document_company_names, 'Denomination', '')
            if len(name) == 0:
                document_company_names = mdb.find_company_name_denomination(vat, company_names_denomination_collection, 1)
                name = self.get_value_from_document_or_default(document_company_names, 'Denomination', '')
                if len(name) == 0:
                    document_company_names = mdb.find_company_name_denomination(vat, company_names_denomination_collection, 2)
                    name = self.get_value_from_document_or_default(document_company_names, 'Denomination', '')

            if (len(name)>0):
                print('name found: '+name)
            url_input = self.get_value_from_document_or_default(document_from_url_collection,'url','')


            # second attempt
            print('url_input {} | vat {}: '.format(url_input,vat))
            if (url_input.strip() == ''):
                print('url_input.strip() == ''')
                document_from_url_collection = mdb.find_company_contact(vat, contacts_collection, ContactyType.WEB.value)
                print('(len(url_input) == 0): '+str(document_from_url_collection))
                url_input = self.get_value_from_document_or_default(document_from_url_collection,'Value','')

            # if len(url_input)==0:
            #     search_term='{} {}'.format(source_name,city)
            #     print('google search for "{}"'.format(search_term))
            #     url_input=scraper.scrape_online_search_engine_with_keyword_return_first_url(search_term=search_term, number_results=100, language_code='en')
            #     print('google search for "{}" returned for url_input "{}"'.format(search_term,url_input))

            #another attempt from google places api
            if len(url_input.strip())==0:
                if len(source_name)==0:
                    search_term = '{} {}'.format(name, city)
                else:
                    search_term = '{} {}'.format(source_name, city)
                url_input=scraper.scrape_google_places_json_api(search_term)
                accessed_google_places+=1

            if url_input is not None and len(url_input)>0:
                mdb.update_one_field_with_vat_as_key(urls_collection,vat,'url',url_input)
                email_list=scraper.scrape_only_email_from_scraped_content_in_db_or_scrape_first(vat, url_input, urls_collection,scraped_content_collection)

            if (len(email_list)>0):
                emails_as_string=str(email_list).replace('{','').replace('}','').replace("'","").replace(';',',')


            #get document_company_names
            #see code.csv for legend:
            # TypeOfDenomination, "001", "FR", "DÃ©nomination sociale"
            # TypeOfDenomination, "001", "NL", "Maatschappelijke naam"
            # TypeOfDenomination, "002", "FR", "AbrÃ©viation"
            # TypeOfDenomination, "002", "NL", "Afkorting"
            # TypeOfDenomination, "003", "FR", "DÃ©nomination commerciale"
            # TypeOfDenomination, "003", "NL", "CommerciÃ«le naam"


            #second attempt (except for url_input which we attempted already a 2nd time)
            if (len(email_list)==0):
                document_from_url_collection = mdb.find_company_contact(vat, contacts_collection, ContactyType.EMAIL.value)
                print('(len(email_list)==0): '+str(document_from_url_collection))
                emails_as_string = self.get_value_from_document_or_default(document_from_url_collection,'Value','')


            if (len(phone) == 0):
                document_from_url_collection = mdb.find_company_contact(vat, contacts_collection, ContactyType.TEL.value)
                print('(len(phone) == 0): '+str(document_from_url_collection))
                phone=self.get_value_from_document_or_default(document_from_url_collection,'Value','')
            # print(vat)
            # print(source_name)
            # print(name)
            # print(url_input)
            # print(emails_as_string)
            # print(phone)
            # print(street)
            # print(houseno)
            # print(box)
            # print(city)
            # print(zipcode)
            # print(country)
            utilities.append_text_to_text_file(output_csv_file, '\n' + str(vat + seperator + source_name + seperator + name + seperator + url_input + seperator + emails_as_string + seperator + phone + seperator + street + seperator + houseno + seperator + box + seperator + city + seperator + zipcode + seperator + country + seperator), 'a')
        print('Done (accessed Google Places API {} times)'.format(accessed_google_places))

    def import_content_from_kbo_csv_file(self, csv_file, collection_name):
        row_count = sum(1 for line in open(csv_file, 'r', encoding='utf-8'))-1
        df = pd.read_csv(csv_file, header=0)
        df['EntityNumber']= 'BE'+df['EntityNumber'].str.strip().str.replace('.','')
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        print('Thread {} | {} lines to import found'.format(threading.get_ident(),row_count))
        mdb.import_all_content_from_pandas_dataframe__in_collection(df, collection_name)
        cursor=mdb.get_all_companies(collection_name,True)
        records_count=cursor.count()
        cursor.close()
        print('Thread {} | Importing of {} lines done. Current size of collection {} is {}'.format(threading.get_ident(),row_count,collection_name,records_count))


    def get_value_from_document_or_default(self,document,key,default):
        value_to_return=default
        try:
            value_to_return = document[key]
        except Exception as e:
            print('Exception in get_value_from_document_or_default(: {}'.format(str(e)))
            pass
        return value_to_return

    def generate_csv_list_with_vat_and_latlong_and_url(self, app:Flask,latlong_collection:str,url_collection:str,output_csv_file,vat_array):
        buffer=1000
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        current_row = 0
        delimiter=';'
        #row_count = len(open(input_csv_file).readlines())
        row_count=len(vat_array)
        print('Thread {} | total rows found: {}'.format(threading.get_ident(),str(row_count)))
        if app is not None:
            print_and_emit(app, 'Thread {} | total rows found: {}'.format(threading.get_ident(),str(row_count)), EVENT, NAMESPACE)
        if (row_count==0):
            print('{} rows found for given array, stopping'.format(row_count))
            return None

        #with open(input_csv_file, 'r',encoding='utf-8') as csvfile:
        if 1==1:  #this line can be removed later, temporarily leaving it against indentation errors
            header_row='vat'+delimiter+'lat'+delimiter+'long'+delimiter+'url'+delimiter
            current_generated_line = ''
            current_generated_line+=header_row
            #csvreader = csv.reader(csvfile, delimiter=input_csv_file_delimiter)
            # some files we use for input, have the vat for some reaons multiple times, we want it just once in our output
            #set for fast O(1) amortized lookup
            seen = set()
            cursor=mdb.find_latlong_with_given_vat_array(latlong_collection,vat_array,True)

            all_latlongs_list=list(cursor)
            cursor.close()
            for vat in vat_array:
                current_row += 1
                if current_row == 1:
                    continue  # the first and second row is a row with headers
                #vat = row[input_csv_file_vat_column]
                if len(vat)==0:
                    print('Thread {} | no vat found for row {} of {}'.format(threading.get_ident(),current_row,row_count))
                    continue
                if vat in seen:
                    print('Thread {} | we saw that vat ({}) already for row {} of {}'.format(threading.get_ident(),vat, current_row, row_count))
                    continue
                seen.add(vat)
                current_generated_line+='\n'
                vat=re.sub("(?i)be","BE", vat)  #replace be to BE (case insensitive)
                if not vat.startswith('BE'):
                    vat='BE'+vat.replace('.','')
                current_generated_line+=vat+delimiter
                #document_lat_long=mdb.find_company(vat,latlong_collection)
                document_lat_long= next((x for x in all_latlongs_list if x['vat']==vat), None)
                lat = ''
                long = ''
                if document_lat_long is not None:
                    try:
                        lat=str(document_lat_long['location']['coordinates'][1])
                        long=str(document_lat_long['location']['coordinates'][0])
                    except:
                        pass
                current_generated_line+=lat+delimiter+long+delimiter

                #now we collect the url
                document_url = mdb.find_company(vat, url_collection)
                url=''
                if document_url is not None:
                    try:
                        url=document_url['url']
                    except:
                        pass
                current_generated_line += url + delimiter
                if current_generated_line.count('\n') % buffer ==0:
                    utilities.append_text_to_text_file(output_csv_file, current_generated_line, mode='a')
                    # clear for next round:
                    current_generated_line = ''
                print('thread {} | Processed row {} of {} | current line: {}'.format(threading.get_ident(),current_row,row_count,current_generated_line.replace('\n','')))
                if app is not None and current_row % 10 == 0:
                    print_and_emit(app,'thread {} | Processed row {} of {} | current line: {}'.format(threading.get_ident(),
                                                                                         current_row, row_count,
                                                                                         current_generated_line.replace(
                                                                                             '\n', '')),EVENT,NAMESPACE)

                #break
            if len(current_generated_line) > 0:
                utilities.append_text_to_text_file(output_csv_file, current_generated_line, mode='a')
            if app is not None:
                print_and_emit(app,
                               'thread {} | Processed {} rows'.format(threading.get_ident(),
                                                                                              row_count), EVENT,
                               NAMESPACE)








