# voeruit Add_lat_long_to_scholen
import csv
import datetime
import json
import logging
import logging.handlers

import os

#from companies_model_editor.Update_companies_in_mongoDB import Update_companies_in_DB
import threading
from os.path import isfile, join

from companies_model_editor.Update_companies_in_mongoDB import Update_companies_in_DB
from easy_order.MongoDB_Utilities import MongoDB_utilities
from easy_order.UpdateOrInsertSchoolInMongoDB import UpdateOrInsertSchoolInMongoDB
from easy_order.add_lat_long_to_scholenlijst import Add_lat_long_to_scholen_class
from models.GeoJSON import geo_json
from utilities import utilities, geography_utilities
from utilities.BSearchScraper import BSearchScraper
from utilities.DTM_Creator import DTM_Creator
from utilities.email_sender import Email_sender
from utilities.Scraper import Scraper
from utilities.geography_utilities import calcuclate_distance_between_given_coords
from utilities.utilities import  init_logger

VLAAMSE_SCHOLEN_CSV_FILE = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\scholen\\vlaamse_scholen.csv'
BRUSSEL_NL_SCHOLEN_CSV_FILE = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\scholen\\brussel_nederlanstalige-scholen.csv'
BRUSSEL_FR_SCHOLEN_CSV_FILE = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\scholen\\brussel_french-language-schools.csv'
OUTPUT_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\scholen\\output\\'
LOGS_DIR=OUTPUT_DIR
TIMESTAMP = datetime.datetime.now().isoformat().replace(':', '_')
SEP = ';'
file_path=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\scholen\\alle_scholen.csv'

MONDOGDB_SERVER = 'localhost'
MONGODB_PORT = 27017
MONGODB_DB = 'companies_db'

#some collection names:
COLLECTION_FULL_ADDRESSES="full_addresses"
COLLECTION_CONTACTS="contacts"
COLLECTION_LATLONG_COORDS="latlong_coordinates"
COLLECTION_URLS="urls"
COLLECTION_RECORDED_EMPLOYEES="recorded_employees"
COLLECTION_SCRAPED_CONTENT="scraped_content"
COLLECTION_COMPANY_NAMES_DENOMINATION= "company_names_denomination" #in kbo it is called denomination


def _collect_latlong_from_scholen_and_export_to_csv():
    schools = []
    test = Add_lat_long_to_scholen_class(OUTPUT_DIR)
    schools = schools + test.collect_vlaamse_scholen(VLAAMSE_SCHOLEN_CSV_FILE)
    schools = schools + test.collect_brussels_schools(BRUSSEL_NL_SCHOLEN_CSV_FILE, BRUSSEL_FR_SCHOLEN_CSV_FILE)
    headers = 'net' + SEP + 'name' + SEP + 'street' + SEP + 'houseno' + SEP + 'postcode' + SEP + 'city' + SEP + 'phone' + SEP + 'email' + SEP + 'url' + SEP + 'lat' + SEP + 'long' + SEP + 'last_updated'
    utilities.save_list_to_csv_file(OUTPUT_DIR + 'alle_scholen_' + TIMESTAMP + '_.csv', headers, schools, 'r')


def add_scholen_to_mongodb():
    test = UpdateOrInsertSchoolInMongoDB( 'localhost', 27017, 'schools_db', "collection")
    test.clearDB()
    test.add_schools_from_csv_to_mongodb(file_path, True)

def calculate_distance_and_export_to_csv():
    schools = []
    test = UpdateOrInsertSchoolInMongoDB( 'localhost', 27017, 'schools_db', "collection")
    original_lat = 50.8515102
    original_long = 2.8281068
    school=test.find_school(original_lat, original_long, 500000)
    calculated_distance = calcuclate_distance_between_given_coords(coords_1=(original_lat, original_long),
                                                                   coords_2=(school.lat, school.long))
    schools.append(school.to_csv(';')+SEP+str(calculated_distance)+SEP)
    headers = 'net' + SEP + 'name' + SEP + 'street' + SEP + 'houseno' + SEP + 'postcode' + SEP + 'city' + SEP + 'phone' + SEP + 'email' + SEP + 'url' + SEP + 'lat' + SEP + 'long' + SEP + 'last_updated'+SEP+'distance'+SEP
    utilities.save_list_to_csv_file(OUTPUT_DIR ,'scholen_in_nabijheid_'+TIMESTAMP+'.csv', headers, schools,mode='w')
    print(school.to_csv(';'))

    #my_logger.info('{}'.format(school.to_csv(';')))
    print('original coords: ', original_lat, original_long)
    print('returned coords: ', school.lat, school.long)
    print(calculated_distance)

def collect_btws_from_given_list_and_add_coords_to_db(input_csv_list,vat_column_id,db_full_address_collection,db_coords_collection):
        mdb = MongoDB_utilities(MONDOGDB_SERVER, MONGODB_PORT, MONGODB_DB)
        current_item_count=0
        row_count = sum(1 for line in open(input_csv_list,encoding="utf-8" ))
        with open(input_csv_list, 'r', encoding='utf-8') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=';')
            for row in csvreader:
                current_item_count += 1
                if current_item_count == 1:
                    print(
                        'skipping for current_row is headerrow | no. {} of {}'.format(current_item_count, row_count))
                    continue  # the first and second row is a row with headers

                vat = row[vat_column_id]
                if len(vat) == 0:
                    print(
                        'skipping since no vat found | no. {} of {}'.format(current_item_count, row_count))
                    continue
                vat = 'BE' + vat.strip(' \n').replace('\r', '').replace('.', '')
                if mdb.find_company(vat, COLLECTION_LATLONG_COORDS) is not None:
                    print(
                        'skipping since latlong already found for vat {}| no. {} of {}'.format(vat, current_item_count, row_count))
                    continue
                document = mdb.find_company(vat, COLLECTION_FULL_ADDRESSES)
                if document is None:
                    print(
                        'skipping since no address found for vat {}| no. {} of {}'.format(vat, current_item_count,
                                                                                           row_count))
                    continue
                print(document)
                street = document["street"]
                houseno = document["houseno"]
                postcode = document["zipcode"]
                city = document["city"]
                address = str(street + ' ' + str(houseno) + ' ,' + str(postcode) + ' ' + city)
                lat_long_string = geography_utilities.get_lat_long_all_methods(address)
                if lat_long_string is not None:
                    lat = float(lat_long_string.split(',')[0])
                    long = float(lat_long_string.split(',')[1])
                else:
                    print(
                            'not added since no coords found | no. {} of {} | vat {} | data: None added'.format(
                                current_item_count,
                                row_count,
                                vat))
                    continue
                new_lat_long_data = {}
                new_lat_long_data["vat"] = vat
                new_lat_long_data["location"] = geo_json("Point", lat=lat, long=long).to_dict()
                mdb.insert_item_with_vat_as_key(COLLECTION_LATLONG_COORDS, new_lat_long_data, True)
                print(
                        'added | no. {} of {} | vat {} | data: {} '.format(current_item_count, row_count,
                                                                           vat,
                                                                           new_lat_long_data))


def collect_btws_from_given_db_and_add_coords_to_db(input_db_collection, db_full_address_collection,
                                                      db_coords_collection):
    mdb = MongoDB_utilities(MONDOGDB_SERVER, MONGODB_PORT, MONGODB_DB)
    current_item_count = 0
    cursor = mdb.get_all_companies(input_db_collection,timeout=True)
    row_count = cursor.count()
    print("found a total of {} rows to process".format(row_count))
    for input_document in cursor:
        current_item_count += 1
        vat = input_document["vat"]
        if len(vat) == 0:
            print(
                'skipping since no vat found | no. {} of {}'.format(current_item_count, row_count))
            continue
        if mdb.find_company(vat,COLLECTION_LATLONG_COORDS) is not None:
            print(
                'skipping since coords known already found for vat {} | no. {} of {}'.format(vat,
                                                                                                    current_item_count,
                                                                                                    row_count))
            continue
        document=mdb.find_company(vat,db_full_address_collection)
        if document == None:
            print(
                'skipping since no document in db addresses found for vat {} | no. {} of {}'.format(vat,current_item_count, row_count))
            continue
        street = document["street"]
        houseno = document["houseno"]
        postcode = document["zipcode"]
        city = document["city"]
        address = str(street + ' ' + str(houseno) + ' ,' + str(postcode) + ' ' + city)
        lat_long_string = geography_utilities.get_lat_long_all_methods(address)
        if lat_long_string is not None:
            lat = float(lat_long_string.split(',')[0])
            long = float(lat_long_string.split(',')[1])
        else:
            print(
                'not added since no coords found | no. {} of {} | vat {} | data: None added'.format(
                    current_item_count,
                    row_count,
                    vat))
            continue
        new_lat_long_data = {}
        new_lat_long_data["vat"] = vat
        new_lat_long_data["location"] = geo_json("Point", lat=lat, long=long).to_dict()
        mdb.insert_item_with_vat_as_key(db_coords_collection, new_lat_long_data, True)
        print(
            'added | no. {} of {} | vat {} | data: {} '.format(current_item_count, row_count,
                                                               vat,
                                                               new_lat_long_data))

        cursor.close()
#calculate distance etc


def convert_old_urlnames_to_vat_names_for_scraped_content(old_dir,new_dir):
    number_of_input_files = len(
        [f for f in os.listdir(old_dir) if isfile(join(old_dir, f))])
    number_of_processed = 0
    for file in [f for f in os.listdir(old_dir) if isfile(join(old_dir, f))]:
        number_of_processed += 1
        print('Thread {} | JsonFile: {} | Processing {} from {}'.format(threading.get_ident(), file,
                                                                        number_of_processed,
                                                                        number_of_input_files))
        with open(old_dir + file, encoding='utf-8', errors='ignore') as f:
            for line in f:
                # load valid lines (should probably use rstrip)
                # if len(line) < 10: continue
                try:
                    # print(collection)
                    new_data = {}
                    old_data = json.loads(line)
                    print(old_data)
                    new_data["vat"] = old_data["vat"]
                    new_data["url_parsed"] = old_data["url_parsed"]
                    new_data["scrapedContent"] = old_data["scrapedContent"]
                    new_data["url_not_parsed"] = old_data["url_not_parsed"]
                    try:
                        new_data["lastupdated"] = old_data["lastupdated"]
                    except:
                        new_data["lastupdated"] = str(datetime.datetime.now())
                    if not os.path.exists(new_dir):
                        os.makedirs(new_dir)
                    f_result_nl = open(new_dir+new_data["vat"]+ str('.json'), 'w', encoding="utf-8")  # https://stackoverflow.com/a/42495690
                    # f_result_nl.write(current_url + '\n' + str(scraped_text))
                    f_result_nl.write(json.dumps(new_data))  # based on https://stackoverflow.com/a/10252138/7194726
                    # f_result_nl.write(jsonpickle.encode(scrapedUrlObject)) #based on https://stackoverflow.com/a/10252138/7194726
                    f_result_nl.close()
                except Exception as e:
                    print('Exception occured: {}'.format(str(e)))


if __name__ == "__main__":
    #calculate_distance_and_export_to_csv()
    input_dir=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\scrapedUrls\\'
    output_dir=input_dir+'\\output\\'
    annual_accounts_csv_file=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\ReceivedRawLists\\annual_accounts_2015_2016.csv'

    kbo_address_csv_file=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\KboOpenData_0046_2017_11_Full\\address.csv'
    kbo_contact_csv_file=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\KboOpenData_0046_2017_11_Full\\contact.csv'
    kbo_company_names_csv_file=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\KboOpenData_0046_2017_11_Full\\denomination.csv'

    bakkerijen_csv_file=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\target_list_easy_order.csv'
    scholen_csv_file=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\scholen voor easy order.csv'
    downloads_dir=r'C:\\Users\\alain\\Downloads\\'
    makro_csv_file=r'C:\\Users\\alain\\Downloads\\Metro_no contactdetails_TEST_scraping_20180712.csv'
    kbo_denomination_file=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\KboOpenData_0046_2017_11_Full\\denomination.csv'

    output_dir=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\output\\'
    output_bakkerijen_csv_file=output_dir+'target_list_easy_order_output.csv'
    output_emails_csv_file=output_dir+'emails_output.csv'
    output_scholen_csv_file=output_dir+'scholen voor easy order_output.csv'
    output_annual_accounts_csv_file=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\annual_accounts_2015_2016_output.csv'
    dtm_output_csv = output_dir+'dtm_{}.csv'.format(TIMESTAMP)
    find_all_mailaddresses_output=output_dir+'all_emails_{}.csv'.format(TIMESTAMP)

    dir_with_scraped_bsearch_urls_as_jsons= r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\Companies\\annual_accounts_2015_2016\\companies\\'
    dir_with_old_scraped_urls_to_convert_to_new_model= r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\scrapedUrls\\microsoft\\'
    scraper_output_dir= r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\scrapedUrls\\easy_order\\'
    output_400k_with_latlong=output_dir+'400k_with_latlong_{}.csv'.format(TIMESTAMP)

    test=Update_companies_in_DB(MONDOGDB_SERVER,MONGODB_PORT,MONGODB_DB)

    #test.add_full_address_from_kbo_csv_file(COLLECTION_FULL_ADDRESSES,kbo_address_csv_file,check_if_record_exist=True,sep=',',threads=32)
    #test.add_contact_from_kbo_csv_file(COLLECTION_CONTACTS,kbo_contact_csv_file,sep=',')
    #test.add_company_names_from_kbo_csv_file(COLLECTION_COMPANY_NAMES,kbo_company_names_csv_file,sep=',')
    #test.add_employees_recorded(annual_accounts_csv_file,True,COLLECTION_RECORDED_EMPLOYEES)
    #test.add_latlong(db_latlong_collection=COLLECTION_LATLONG_COORDS,db_full_address_collection=COLLECTION_FULL_ADDRESSES,number_to_skip_of_first_results=193000)
    #test.add_latlong_with_threads(db_latlong_collection=COLLECTION_LATLONG_COORDS,db_companies_collection=COLLECTION_FULL_ADDRESSES,threads=10)

    #test.add_latlong_with_other_then_companies_db(db_latlong_collection=COLLECTION_LATLONG_COORDS,db_full_address_collection=COLLECTION_FULL_ADDRESSES,db_not_a_full_address_collection=COLLECTION_URLS,number_to_skip_of_first_results=188516)
    #test.add_latlong_with_other_then_companies_db(db_latlong_collection=COLLECTION_LATLONG_COORDS,db_full_address_collection=COLLECTION_FULL_ADDRESSES,db_not_a_full_address_collection=COLLECTION_URLS,number_to_skip_of_first_results=290325)
    #test.add_latlong_with_other_then_companies_db(db_latlong_collection=COLLECTION_LATLONG_COORDS,db_full_address_collection=COLLECTION_FULL_ADDRESSES,db_not_a_full_address_collection=COLLECTION_URLS,number_to_skip_of_first_results=361840)
    #test.add_latlong_with_other_then_companies_db(db_latlong_collection=COLLECTION_LATLONG_COORDS,db_full_address_collection=COLLECTION_FULL_ADDRESSES,db_not_a_full_address_collection=COLLECTION_URLS,number_to_skip_of_first_results=398874)

    #test.add_latlong_with_other_then_companies_db(db_latlong_collection=COLLECTION_LATLONG_COORDS,db_full_address_collection=COLLECTION_FULL_ADDRESSES,db_not_a_full_address_collection=COLLECTION_URLS,number_to_skip_of_first_results=300000)

    #collect_btws_from_given_db_and_add_coords_to_db(input_db_collection=COLLECTION_RECORDED_EMPLOYEES,db_full_address_collection=COLLECTION_FULL_ADDRESSES,db_coords_collection=COLLECTION_LATLONG_COORDS)

    #test.find_company_within_coords_distance(db_latlong_collection=COLLECTION_LATLONG_COORDS,lat=51.162943, long=4.422861,min_distance_meters=0)
    #test.add_url_from_bsearch_with_csv_as_input(COLLECTION_URLS,bakkerijen_csv_file, 0)
    vat_array=utilities.get_vat_array_from_csv_file(makro_csv_file,3)
    #vat_array=utilities.get_vat_array_from_csv_file(downloads_dir+'input for call list part 2.csv',3)
    scraper=Scraper(None,MONDOGDB_SERVER,MONGODB_PORT,MONGODB_DB)
    bsearch_scraper=BSearchScraper(MONDOGDB_SERVER,MONGODB_PORT,MONGODB_DB)
    #bsearch_scraper.scrape_bsearch_with_vat_array(vat_array, COLLECTION_URLS)
    #test.add_url_from_bsearch_with_csv_as_input(COLLECTION_URLS,scholen_csv_file, 0)
    #test.delete_documents_with_empty_field(COLLECTION_RECORDED_EMPLOYEES,field_name="recorded_employees")
    #test.reindex(COLLECTION_FULL_ADDRESSES)
    #collect_btws_from_given_list_and_add_coords_to_db(bakkerijen_csv_file,0,COLLECTION_FULL_ADDRESSES,COLLECTION_LATLONG_COORDS)
    #test.add_url_from_bsearch_with_jsons_as_input(COLLECTION_URLS, dir_with_scraped_bsearch_urls_as_jsons)
    #BETER NIET GEBRUIKEN. MAAR WEL OPNIEUW SCRAPEN test.add_to_db_from_older_scraped_url_contents(collection_companies=COLLECTION_FULL_ADDRESSES,db_urls_collection="test_first_scraped_stuff_conversion",old_url_files_dir=dir_with_old_scraped_urls_to_convert_to_new_model)
    #test.generate_csv_list_with_vat_and_latlong(latlong_collection=COLLECTION_LATLONG_COORDS,url_collection=COLLECTION_URLS,input_csv_file=bakkerijen_csv_file,input_csv_file_delimiter=';',input_csv_file_vat_column=0,output_csv_file=output_bakkerijen_csv_file)
    #test.generate_csv_list_with_vat_and_latlong(latlong_collection=COLLECTION_LATLONG_COORDS,url_collection=COLLECTION_URLS,input_csv_file=scholen_csv_file,input_csv_file_delimiter=';',input_csv_file_vat_column=0,output_csv_file=output_scholen_csv_file)
    #test.generate_csv_list_with_vat_and_latlong(latlong_collection=COLLECTION_LATLONG_COORDS,url_collection=COLLECTION_URLS,input_csv_file=annual_accounts_csv_file,input_csv_file_delimiter=',',input_csv_file_vat_column=1,output_csv_file=output_annual_accounts_csv_file)

    #vat_array=utilities.get_vat_array_from_csv_file(output_annual_accounts_csv_file,0)
    #vat_array=utilities.get_vat_array_from_csv_file(bakkerijen_csv_file,0)
    #vat_array=['BE0207559115','BE0400123812','BE0400369676', 'BE0211376955']
    #test.generate_csv_list_with_vat_and_latlong_and_url(None,latlong_collection=COLLECTION_LATLONG_COORDS,url_collection=COLLECTION_URLS,output_csv_file=output_400k_with_latlong,vat_array=vat_array)

    # convert_old_urlnames_to_vat_names_for_scraped_content(old_dir=scraper_output_dir,new_dir=scraper_output_dir+'new\\')
    #scraper.scrape_with_db_if_url_field_exists(COLLECTION_URLS,COLLECTION_SCRAPED_CONTENT)
    #vat_url_list=utilities.get_vat_url_list_from_csv_file(output_dir + 'emails_output_2018-07-31T11_53_00.119730.csv', vat_column_id=0, url_column_id=3)
    #scraper.scrape_urls_from_vat_urls_list_to_db(vat_urls_list=vat_url_list, collection_with_scraped_content=COLLECTION_SCRAPED_CONTENT, collection_with_urls=COLLECTION_URLS,number_of_threads=1)
    ##test.generate_csv_list_from_csv_input_with_output_of_vat_url_emails_phone_etc(downloads_dir+'input for call list part 2.csv',3,2, COLLECTION_URLS, COLLECTION_SCRAPED_CONTENT, COLLECTION_CONTACTS, COLLECTION_FULL_ADDRESSES, COLLECTION_COMPANY_NAMES_DENOMINATION, output_dir+'emails_output_{}.csv'.format(TIMESTAMP), ';')
    test.generate_csv_list_from_csv_input_with_output_of_vat_url_emails_phone_etc(makro_csv_file,3,4, COLLECTION_URLS, COLLECTION_SCRAPED_CONTENT, COLLECTION_CONTACTS, COLLECTION_FULL_ADDRESSES, COLLECTION_COMPANY_NAMES_DENOMINATION, output_dir+'metro_output_{}.csv'.format(TIMESTAMP), ';')

    #vat_array = utilities.get_vat_array_from_csv_file(downloads_dir+'input for call list part 2.csv', 3)
    #test.save_document_contents_in_bear_texts(COLLECTION_SCRAPED_CONTENT,vat_array=vat_array,output_dir=output_dir+'scraped_content_{}\\'.format(TIMESTAMP),field_list_to_incl_in_bear_text=['scrapedContent'])
    #test.create_index_for_vat(COLLECTION_SCRAPED_CONTENT)
    #test.add_scraped_url_contents_from_json_files(urls_collection=COLLECTION_URLS,db_scraped_urls_collection=COLLECTION_SCRAPED_CONTENT,input_dir_with_json_files=scraper_output_dir)
    # test.create_index_for_vat(COLLECTION_SCRAPED_CONTENT)
    #test.delete_documents_with_given_array(COLLECTION_URLS,vat_array)

    # vat_array=utilities.get_vat_array_from_csv_file(bakkerijen_csv_file,0)
    #test.collect_all_email_address(output_full_path=find_all_mailaddresses_output,db_collection=COLLECTION_SCRAPED_CONTENT,optional_vat_array=None)
    #test.collect_all_email_address(output_full_path=find_all_mailaddresses_output,db_collection=COLLECTION_SCRAPED_CONTENT,optional_vat_array=vat_array)

    #test.import_content_from_kbo_csv_file(kbo_denomination_file, COLLECTION_COMPANY_NAMES_DENOMINATION)
    #test.import_content_from_kbo_csv_file(kbo_contact_csv_file, COLLECTION_CONTACTS)

    keywords_list = ['deliveroo', 'facebook', 'ubereats.com', 'takeaway.com', 'one2three.be', 'Joyn', 'Localtomorrow',
                     'foodora', 'online bestellen', 'delivery', 'aan huis', 'bezorging', 'bestellen', 'frituur',
                     'broodjes', 'filialen', 'bestel online', 'afhalen', 'leveren',
                     'commande en ligne', 'livraison', 'à domicile', 'livraison', 'commande',
                     'friture', 'sandwiches','affiliés', 'commander en ligne', 'ramasser', 'livrer']
    # dtm_creator=DTM_Creator(MONDOGDB_SERVER,MONGODB_PORT,MONGODB_DB)
    #
    # # # with scraped content of all urls
    # dtm_creator.create(dtm_output_csv,COLLECTION_SCRAPED_CONTENT,keywords_list,None)

    #now with a specific given vat list
    #dtm_creator.create(dtm_output_csv,COLLECTION_SCRAPED_CONTENT,keywords_list,vat_array)

    #email_sender=Email_sender('localhost','ger@gm.com','info@gershon-lehrer.be',output_annual_accounts_csv_file)