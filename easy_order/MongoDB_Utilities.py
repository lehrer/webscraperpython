import csv
import json
import re
import threading
import pandas as pd
from os.path import isfile, join

import os

import datetime
import pymongo
from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError

from models import ContactType
from models.GeoJSON import geo_json
from utilities import geography_utilities
from utilities.geography_utilities import get_lat_long_all_methods


class MongoDB_utilities:
    def __init__(self, mongodb_server,mongodb_port,mongodb_db):
        self.mongodb_server = mongodb_server
        self.mongodb_port = mongodb_port
        self.mongodb_db = mongodb_db

        client = MongoClient(mongodb_server, mongodb_port)
        self.mongodb_client = client[mongodb_db]



    def find_company(self,vat, mongodb_collection_companies):
        #regx = re.compile(vat, re.IGNORECASE)
        #for document in self.mongodb[mongodb_collection_companies].find({'vat': regx}):
        for document in self.mongodb_client[mongodb_collection_companies].find({'vat': vat}):
            return document
        #print(self.mongodb, mongodb_collection_companies)
        return None


    def find_company_contact(self, vat, mongodb_collection_contacts, type:ContactType):
        #regx = re.compile(vat, re.IGNORECASE)
        #for document in self.mongodb[mongodb_collection_companies].find({'vat': regx}):
        print("query: 'EntityNumber': '{}','ContactType':'{}'".format(vat,str(type)))
        for document in self.mongodb_client[mongodb_collection_contacts].find({'EntityNumber': vat, 'ContactType':str(type)}):
            return document
        #print(self.mongodb, mongodb_collection_companies)
        return None

    def find_company_name_denomination(self, vat, mongodb_collection_denomination, TypeOfDenomination:int):
        #regx = re.compile(vat, re.IGNORECASE)
        #for document in self.mongodb[mongodb_collection_companies].find({'vat': regx}):
        print("query: 'EntityNumber': '{}','TypeOfDenomination':{}".format(vat,TypeOfDenomination))
        for document in self.mongodb_client[mongodb_collection_denomination].find({'EntityNumber': vat, 'TypeOfDenomination':TypeOfDenomination}):
            return document
        #print(self.mongodb, mongodb_collection_companies)
        return None

    def find_scraped_content(self,keyword, mongodb_collection_scraped_content,timeout:bool):
        regx = re.compile(keyword, re.IGNORECASE)
        cursor= self.mongodb_client[mongodb_collection_scraped_content].find({'scrapedContent': regx}, no_cursor_timeout=timeout)
        return cursor

    def find_scraped_content_with_given_vat_array(self,keyword, mongodb_collection_scraped_content,vat_array,timeout:bool):
        regx = re.compile(keyword, re.IGNORECASE)
        print('Thread {} | There are {} vats'.format(threading.get_ident(),len(vat_array)))
        cursor= self.mongodb_client[mongodb_collection_scraped_content].find({'vat':{'$in':vat_array}, 'scrapedContent': regx}, {'vat': 1, 'lastupdated': 1, 'scrapedContent': 1, 'url_not_parsed':1, 'url_parsed':1}, no_cursor_timeout=timeout)
        print('size of cursor {}'.format(cursor.count()))
        return cursor


    def find_latlong_with_given_vat_array(self,mongodb_collection,vat_array,timeout:bool):
        print('Thread {} | There are {} vats'.format(threading.get_ident(),len(vat_array)))
        cursor= self.mongodb_client[mongodb_collection].find({'vat':{'$in':vat_array}}, no_cursor_timeout=timeout)
        print('size of cursor {}'.format(cursor.count()))
        return cursor



    def find_latlong_and_url_TEST_DID_NOT_GIVE_WANTED_RESULT(self,vat,mongodb_collection_companies,mongodb_collection_urls):
        regx = re.compile(vat, re.IGNORECASE)
        for document in self.mongodb_client[mongodb_collection_companies]\
            .aggregate([{'$lookup': {'from': mongodb_collection_urls,
                                     'localField': "vat", 'foreignField': "vat",
                                     'as': "company_docs"}},
                        {'$match': {"company_docs": {"vat": regx}}}]):
            return document
        return None


    def find_scraped_content_with_given_url_unparsed(self,unparsed_url, mongodb_collection_scraped_content):
        unparsed_url=unparsed_url.replace('(','\(').replace(')','\)')
        regx = re.compile(unparsed_url, re.IGNORECASE)
        for document in self.mongodb_client[mongodb_collection_scraped_content].find({'url_not_parsed': regx}):
            return document
        #print(self.mongodb, mongodb_collection_companies)
        return None

    def find_company_with_given_url_unparsed(self,url, mongodb_collection_companies):
        regx = re.compile(url, re.IGNORECASE)
        for document in self.mongodb_client[mongodb_collection_companies].find({'url': regx}):
            return document
        #print(self.mongodb, mongodb_collection_companies)
        return None

    def find_company_with_given_vat(self,vat,mongodb_collection_companies):
        regx = re.compile(vat, re.IGNORECASE)
        for document in self.mongodb_client[mongodb_collection_companies].find({'vat': regx}):
            return document
        # print(self.mongodb, mongodb_collection_companies)
        return None

    def get_all_companies(self, mongodb_collection_name,timeout:bool):
        cursor = self.mongodb_client[mongodb_collection_name].find({}, no_cursor_timeout=timeout)
        return cursor

    def get_all_companies_with_existing_field(self, mongodb_collection_name,field_name:str,must_exist:bool,timeout:bool):
        cursor = self.mongodb_client[mongodb_collection_name].find({field_name : {"$exists" : must_exist}}, no_cursor_timeout=timeout)
        return cursor

    def add_recorded_employees(self,mongodb_collection_recorded_employees,vat,recorded_employees,upsert:bool):
        counter=0
        new_data = {}
        new_data["vat"] = vat
        new_data["recorded_employees"] = recorded_employees
        try:
            self.mongodb_client[mongodb_collection_recorded_employees].update_one(filter={'vat': new_data["vat"]}, update={"$set": new_data}, upsert=upsert)
        except DuplicateKeyError as dke:
            print("Duplicate Key Error: ", dke)
        except ValueError as e:
            print("Value Error: ", e)
            # friendly log message
            if 0 == counter % 100 and 0 != counter:
                print("loaded line: ", counter)


    def insert_company_in_db(self, json_file: str, db, db_collection, upsert):
        result = db.profiles.create_index([('id', pymongo.ASCENDING)], unique=True)

        # print('Current index: ' + str(sorted(list(db.profiles.index_information()))))

        # A collection is a group of documents stored in MongoDB, and can be thought of as roughly the equivalent of
        # a table in a relational database. Getting a collection in PyMongo works the same as getting a database:
        collection = db.items

        counter = 0
        # print ('json_file: '+json_file)
        with open(json_file, 'r') as f:
            for line in f:

                # load valid lines (should probably use rstrip)
                # if len(line) < 10: continue
                try:
                    # print(collection)
                    new_data = {}
                    old_data = json.loads(line)
                    new_data["id"] = old_data["id"]
                    new_data["vat"] = old_data["vat"]
                    new_data["name"] = old_data["name"]

                    # print(repr(new_data["address"]))
                    new_data["phone"] = old_data["phone"]
                    new_data["url"] = old_data["url"]
                    new_data["lastupdated"] = old_data["lastupdated"]

                    # db[db_collection].insert(new_data)
                    db[db_collection].update_one(filter={'vat': new_data["vat"]}, update={"$set": new_data},
                                                 upsert=upsert)
                except DuplicateKeyError as dke:
                    print("Duplicate Key Error: ", dke)
                except ValueError as e:
                    print("Value Error: ", e)
                    # friendly log message
                    if 0 == counter % 100 and 0 != counter:
                        print("loaded line: ", counter)

            f.close()



    def insert_item_with_vat_as_key(self, db_collection,data, upsert:bool):
        result=None
        try:
            result=self.mongodb_client[db_collection].update_one(filter={'vat': data["vat"]}, update={"$set": data},
                                                                 upsert=upsert)
        except DuplicateKeyError as dke:
            print("Duplicate Key Error: ", dke)
        except ValueError as e:
            print("Value Error: ", e)
            # friendly log message
        return result


    def import_all_content_from_csv_file_in_collection(self, full_path_csv_file, collection):
        db_cm = self.mongodb_client[collection]
        file_res = os.path.join(full_path_csv_file)

        data = pd.read_csv(file_res)
        data_json = json.loads(data.to_json(orient='records'))
        self.mongodb_client[collection].remove( )
        self.mongodb_client[collection].insert(data_json)

    def import_all_content_from_pandas_dataframe__in_collection(self, dataframe, collection):
        data_json = json.loads(dataframe.to_json(orient='records'))
        self.mongodb_client[collection].remove( )
        self.mongodb_client[collection].insert(data_json)




    def update_one_field_with_vat_as_key(self,db_collection,vat,field_to_update,value):
        print('Thread {} | for vat: {} | start updating field: {} | value: {} | collection: {}'.format(threading.get_ident(),vat,field_to_update,value,db_collection))
        result = None
        try:
            result = self.mongodb_client[db_collection].update_one(filter={'vat': vat}, update={"$set": {'vat':vat, field_to_update:value, 'lastupdated':str(datetime.datetime.now())}},
                                                                   upsert=True)
        except DuplicateKeyError as dke:
            print("Duplicate Key Error: ", dke)
        except ValueError as e:
            print("Value Error: ", e)
            # friendly log message
        return result


    def insert_long_lat_to_db(self,vat:str,address:str,type:str, db_collection, upsert):
        # Because this data is geographical, create a 2dsphere index on each collection using the mongo shell:
        new_data = {}
        new_data["vat"]=vat
        new_data['location']['type'] = type
        long=0.0
        lat=0.0

        lat_long_string=get_lat_long_all_methods((address).encode('utf-8').decode('utf-8').replace('&', ''))
        if lat_long_string is not None:
            lat = lat_long_string.split(',')[0]
            long = lat_long_string.split(',')[1]

        new_data['location']['coordinates']=[long,lat]
        try:
            self.mongodb_client[db_collection].update_one(filter={'vat': new_data["vat"]}, update={"$set": new_data},
                                                          upsert=upsert)
        except DuplicateKeyError as dke:
            print("Duplicate Key Error: ", dke)
        except ValueError as e:
            print("Value Error: ", e)
            # friendly log message

    def find_company_with_given_coords(self,db_latlong_collection,lat,long,minimum_distance):
        result = self.mongodb_client[db_latlong_collection].find(
            {"location": {"$near": {"$geometry": {"type": "Point", "coordinates": [long, lat]}, "$minDistance": minimum_distance}}})
        return result

    def add_companies(self,mongodb_collection_companies,json_input_dir):
        number_of_processed=0
        number_of_input_files = len([f for f in os.listdir(json_input_dir) if isfile(join(json_input_dir, f))])
        for file in [f for f in os.listdir(json_input_dir) if isfile(join(json_input_dir, f))]:
            number_of_processed += 1
            print('Thread {} | JsonFile: {} | Processing {} from {}'.format(threading.get_ident(), file,
                                                                            number_of_processed,
                                                                            number_of_input_files))
            with open(json_input_dir + file, encoding='utf-8', errors='ignore') as company_json_file:
                self.insert_company_in_db(json_file=company_json_file.name, db=self.mongodb_client, db_collection=mongodb_collection_companies, upsert=True)

            print('Thread {} | JsonFile: {} | Processed {} from {}'.format(threading.get_ident(), file,
                                                                           number_of_processed,
                                                                           number_of_input_files))


    def create_index_for_vat(self, db_collection):
        self.mongodb_client[db_collection].create_index([("vat", pymongo.ASCENDING)], unique=True)

    def create_index_for_coords(self, db_latlong_collection, name_location_column):
        self.mongodb_client[db_latlong_collection].drop_indexes()
        self.mongodb_client[db_latlong_collection].create_index([(name_location_column, "2dsphere")])

    def reindex_collection(self,db_collection):
        self.mongodb_client[db_collection].reindex()

    def delete_documents_with_empty_field(self, db_collection,field_name):
        self.mongodb_client[db_collection].delete_many({field_name: ""})

    def delete_document_with_given_vat(self, db_collection, vat):
        self.mongodb_client[db_collection].delete_many({'vat': vat})

    def insert_scraped_url_contents_file_in_db(self,json_file: str,  db_collection, upsert):
        counter = 0
        # print ('json_file: '+json_file)
        with open(json_file, 'r') as f:
            for line in f:

                # load valid lines (should probably use rstrip)
                # if len(line) < 10: continue
                try:
                    # print(collection)
                    new_data = {}
                    old_data = json.loads(line)
                    new_data["id"] = old_data["id"]
                    new_data["vat"] = old_data["vat"]
                    new_data["name"] = old_data["name"]
                    new_data["address"] = str(old_data["address"]).strip(' \n').replace('\r', '')
                    # print(repr(new_data["address"]))
                    new_data["phone"] = old_data["phone"]
                    new_data["url"] = old_data["url"]
                    new_data["lastupdated"] = old_data["lastupdated"]

                    # db[db_collection].insert(new_data)
                    self.mongodb_db[db_collection].update_one(filter={'vat': new_data["vat"]}, update={"$set": new_data},
                                                 upsert=upsert)
                except DuplicateKeyError as dke:
                    print("Duplicate Key Error: ", dke)
                except ValueError as e:
                    print("Value Error: ", e)
                    # friendly log message
                    if 0 == counter % 100 and 0 != counter:
                        print("loaded line: ", counter)

            f.close()

