#see also https://docs.mongodb.com/manual/tutorial/geospatial-tutorial/
import csv

from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError

from models.School import School


class UpdateOrInsertSchoolInMongoDB:
    def __init__(self, mongodb_server,mongodb_port,mongodb_db,mongodb_collection):
        self.mongodb_server=mongodb_server
        self.mongodb_port = mongodb_port
        self.mongodb_db = mongodb_db
        self.mongodb_collection = mongodb_collection
        client = MongoClient(mongodb_server, mongodb_port)
        self.mongodb = client[mongodb_db]

    def add_school(self, school, upsert):
        # Because this data is geographical, create a 2dsphere index on each collection using the mongo shell:

        try:
            # print(collection)
            # db[db_collection].insert(new_data)
            self.mongodb[self.mongodb_collection].insert( school.to_dict())
        except DuplicateKeyError as dke:
            print("Duplicate Key Error: ", dke)
        except ValueError as e:
            print("Value Error: ", e)
            # friendly log message

    def add_schools_from_csv_to_mongodb(self, filepath, upsert):
        current_row = 0
        with open(filepath, 'r') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=';')
            for row in csvreader:
                current_row += 1
                if current_row == 1:
                    continue  # the first row is the row with headers
                lat = 0.0
                long = 0.0
                current_row += 1
                if current_row == 1 or current_row == 2:
                    continue  # the first and second row is a row with headers
                #header=net;name;street;houseno;postcode;city;phone;email;url;lat;long;last_updated
                net = row[0]
                name = row[1]
                street = row[2]
                houseno = row[3]
                postcode = row[4]
                city = row[5]
                phone = row[6]
                email = row[7]
                url = row[8]
                try:
                    lat=float(row[9])
                    long=float(row[10])
                except:
                    pass
                lastupdated=row[11]
                school = School(net, name, street, houseno, postcode, city, phone, email, url, lat, long,
                                lastupdated)
                school_dict=[]
                self.mongodb[self.mongodb_collection].drop_indexes()
                self.mongodb[self.mongodb_collection].create_index([("location", "2dsphere")])
                self.add_school(school,upsert)
                print(school.to_dict())
                #print(json.dumps(school.__dict__))
        return None


    def find_school(self,lat:float, long:float, max_distance_meters:float):
        result=self.mongodb[self.mongodb_collection].find({"location": { "$near": { "$geometry": {"type": "Point", "coordinates": [ long,lat]},"$minDistance":0}}})
        # result=db[db_collection].find({"name":"Imelda-Instituut"})
        # regx = re.compile("Imelda-Instituut.*", re.IGNORECASE)
        # result=db[db_collection].find({"name": regx})
        # for document in db[db_collection].find({}, {'_id': False}):
        #     print(document)
        for school_item in result:
            net = school_item['net']
            name = school_item['name']
            street = school_item['street']
            houseno = school_item['houseno']
            postcode = school_item['postcode']
            city = school_item['city']
            phone = school_item['phone']
            email = school_item['email']
            url = school_item['url']
            lat = school_item['location']['coordinates'][1]
            long = school_item['location']['coordinates'][0]
            lastupdated = school_item['lastupdated']
            school_to_return = School(net, name, street, houseno, postcode, city, phone, email, url, lat, long, lastupdated)
            return school_to_return
        print(self.mongodb,self.mongodb_collection)
        return None

    def getDB(self):
        return self.mongodb

    def clearDB(self):
        self.mongodb[self.mongodb_collection].remove({})








