# lijst vlaamse scholen via http://data-onderwijs.vlaanderen.be/onderwijsaanbod/lijst.aspx?hz=1
# lijst Brusselse scholen via https://opendata.brussels.be/explore/dataset/french-language-schools/table/
# en https://opendata.brussels.be/explore/dataset/nederlanstalige-scholen/
import csv
import datetime

from models.School import School
from utilities import geography_utilities
from utilities.utilities import init_logger


class Add_lat_long_to_scholen_class:

    def __init__(self, output_dir):
        self.output_dir=output_dir
        init_logger(output_dir,'logfile.log', 'MyLogger')


    # collect vlaamse scholen (zoek ook latlong op)
    def collect_vlaamse_scholen(self, vlaamse_scholen_csv_file):
        schools = []
        current_row = 0
        with open(vlaamse_scholen_csv_file, 'r',encoding='utf-8') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=';')
            for row in csvreader:
                lat = 0.0
                long = 0.0
                current_row += 1
                if current_row == 1 or current_row==2:
                    continue  # the first and second row is a row with headers
                # reference = row[0]
                net = row[3]
                name = row[4]
                street = row[6]
                houseno = row[7]
                postcode = row[8]
                city = row[9]
                phone = row[13]
                email = row[15]
                url = row[16]

                address = str(street + ' '+ str(houseno) + ' ,' + str(postcode) +' '+ city)
                lat_long_string = geography_utilities.get_lat_long_photon(
                    (address+', '+'Belgium').encode('utf-8').decode('utf-8').replace('&', ''))
                if lat_long_string is None:  # photon did not work, we try again photon without the country:
                    lat_long_string = geography_utilities.get_lat_long_photon(
                        address.encode('utf-8').decode('utf-8').replace('&', ''))
                if lat_long_string is None:#photon did not work at all, we try with locationiq
                    lat_long_string = geography_utilities.get_lat_long_locationiq(
                        (address + ', ' + 'Belgium').encode('utf-8').decode('utf-8').replace('&', ''))


                if lat_long_string is not None:
                    lat = lat_long_string.split(',')[0]
                    long = lat_long_string.split(',')[1]

                print(net,name,street,houseno,postcode,city,phone,email,url,lat,long)
                school = School(net, name, street, houseno, postcode, city, phone, email, url, lat, long,
                                str(datetime.datetime.now()))
                schools.append(school)
            return schools




    # collect brusselse scholen
    def collect_brussels_schools(self, brussel_nl_scholen_csv_file, brussel_fr_scholen_csv_file):
        schools = []
        current_row = 0
        files = [brussel_nl_scholen_csv_file,brussel_fr_scholen_csv_file]
        for f in files:
            with open(f, 'r',encoding='utf-8') as csvfile:
                csvreader = csv.reader(csvfile, delimiter=';')
                for row in csvreader:
                    lat = ''
                    long = ''
                    current_row += 1
                    if row[0].startswith('Beschrijving') or row[0].startswith('Name'):
                        continue  # the first and second row is a row with headers
                    # reference = row[0]
                    name = row[0]
                    lat_long_string=row[1]
                    lat = lat_long_string.split(',')[0]
                    long = lat_long_string.split(',')[1]
                    city = 'Brussel'
                    school = School('', name, '', '', '', city, '', '', '', lat, long,str(datetime.datetime.now()))
                    schools.append(school)
                    print(school.to_csv(';'))
                print(current_row)
        return schools







