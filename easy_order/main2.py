import csv
import datetime
from companies_model_editor.Update_companies_in_mongoDB import Update_companies_in_DB
from utilities.Scraper import Scraper

VLAAMSE_SCHOLEN_CSV_FILE = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\scholen\\vlaamse_scholen.csv'
BRUSSEL_NL_SCHOLEN_CSV_FILE = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\scholen\\brussel_nederlanstalige-scholen.csv'
BRUSSEL_FR_SCHOLEN_CSV_FILE = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\scholen\\brussel_french-language-schools.csv'
OUTPUT_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\scholen\\output\\'
LOGS_DIR=OUTPUT_DIR
TIMESTAMP = datetime.datetime.now().isoformat().replace(':', '_')
SEP = ';'
file_path=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\scholen\\alle_scholen.csv'

MONDOGDB_SERVER = 'localhost'
MONGODB_PORT = 27017
MONGODB_DB = 'companies_db'

#some collection names:
COLLECTION_FULL_ADDRESSES="full_addresses"
COLLECTION_LATLONG_COORDS="latlong_coordinates"
COLLECTION_URLS="urls"

if __name__ == "__main__":
    #calculate_distance_and_export_to_csv()
    input_dir=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\scrapedUrls\\'
    output_dir=input_dir+'\\output\\'
    annual_accounts_csv_file=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\ReceivedRawLists\\annual_accounts_2015_2016.csv'
    kbo_address_csv_file=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\KboOpenData_0046_2017_11_Full\\address.csv'
    bakkerijen_csv_file=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\target_list_easy_order.csv'
    scholen_csv_file=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\scholen voor easy order.csv'

    dir_with_scraped_bsearch_urls_as_jsons= r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\Companies\\annual_accounts_2015_2016\\companies\\'
    dir_with_old_scraped_urls_to_convert_to_new_model= r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\scrapedUrls\\microsoft\\'


    #test=Update_companies_in_DB(MONDOGDB_SERVER,MONGODB_PORT,MONGODB_DB)

    #test.add_full_address_from_kbo_csv_file(COLLECTION_FULL_ADDRESSES,kbo_address_csv_file,check_if_record_exist=True,sep=',')
    #test.add_employees_recorded(annual_accounts_csv_file,True,"recorded_employees")
    #test.add_latlong(db_latlong_collection=COLLECTION_LATLONG_COORDS,db_companies_collection=COLLECTION_FULL_ADDRESSES )
    #collect_btws_from_given_db_and_add_coords_to_db(input_db_collection="recorded_employees",db_full_address_collection=COLLECTION_FULL_ADDRESSES,db_coords_collection=COLLECTION_LATLONG_COORDS)

    #test.find_company_within_coords_distance(db_latlong_collection=COLLECTION_LATLONG_COORDS,lat=51.162943, long=4.422861,min_distance_meters=0)
    #test.add_url_from_bsearch_with_csv_as_input(COLLECTION_URLS,bakkerijen_csv_file, 0)
    #test.add_url_from_bsearch_with_csv_as_input(COLLECTION_URLS,scholen_csv_file, 0)
    #test.delete_documents_with_empty_url(COLLECTION_URLS)
    #test.reindex(COLLECTION_FULL_ADDRESSES)
    #collect_btws_from_given_list_and_add_coords_to_db(bakkerijen_csv_file,0,COLLECTION_FULL_ADDRESSES,COLLECTION_LATLONG_COORDS)
    #test.add_url_from_bsearch_with_jsons_as_input(COLLECTION_URLS, dir_with_scraped_bsearch_urls_as_jsons)
    SCRAPED_CONTENT = "SCRAPED_CONTENT"
    #test.add_to_db_from_older_scraped_url_contents(collection_companies=COLLECTION_FULL_ADDRESSES,db_urls_collection="test_first_scraped_stuff_conversion",old_url_files_dir=dir_with_old_scraped_urls_to_convert_to_new_model)
    #test.add_scraped_url_contents_from_json_files("url","scraped_urls_content",)

    print('lets scrape')
    scraper=Scraper()
    scraper.scrape_with_db(MONDOGDB_SERVER,MONGODB_PORT,MONGODB_DB,COLLECTION_URLS)