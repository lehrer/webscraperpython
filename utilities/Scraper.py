import datetime
import json
import logging
import os
import socket
import threading
import time
import urllib
from urllib.parse import urlsplit
import urllib.parse
from collections import deque
from os.path import isfile, join
from queue import Queue
import lxml.html
from urllib.parse import urlparse
import pprint
import bs4
import requests
import urllib3
import validators
from flask import Flask
from lxml.cssselect import CSSSelector

from easy_order.MongoDB_Utilities import MongoDB_utilities
from models.ScrapedUrl import ScrapedUrl
# constants:
from utilities import utilities
from utilities.utilities import file_exists, print_and_emit
import re

#for flask_socketio
EVENT='message'
NAMESPACE='/'

#specific for scraper etc
SCRAPE_LEVELS = 2
# BASE_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\Get BsearchInfo\\output\\'
TIMESTAMP = datetime.datetime.now().isoformat().replace(':', '_')
# INPUT_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\Companies\\microsoft\\'
#OUTPUT_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\scrapedUrls\\easy_order\\'
#LOGS_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\logs\\'
HEADERS = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
}
THREADS = 32
class Scraper():


    def __init__(self,app:Flask,mongo_server, mongo_port, mongodb):
        self.app=app
        #self.output_dir=output_dir
        self.mongo_server=mongo_server
        self.mongo_port=mongo_port
        self.mongodb=mongodb

        self.number_of_input_records_to_scrape = 0

        self.init_number_of_processed = 0



        # logging (based on https://stackoverflow.com/a/15167862/7194726)
        # if not os.path.exists(LOGS_DIR):
        #     os.makedirs(LOGS_DIR)
        # logging.getLogger('').handlers = []
        # logging.basicConfig(
        #     filename=LOGS_DIR + 'scraper_' + TIMESTAMP + '.log',
        #     format='%(asctime)s %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
        #     filemode="w",
        #     level=logging.DEBUG)

        # variables
        self.already_scraped_list = []  # keep track of what was already scraped
        # myList = []  # list with URL's to scan



    print_lock = threading.Lock()











# read the file with URL's
# f_url = open(BASE_DIR + 'be_searchlist.txt', 'r')
# for line in f_url:
#     myList.append(line.strip('\n'))
# print(myList)
# f_url.close()

# read keywords
# f_url2 = open('/Users/matejkarpisek/Documents/ISV search/ms/wordliste.txt', 'r')
    myList2 = []  # list with URL's to scan


# for line2 in f_url2:
#     myList2.append(line2.strip('\n'))
# # print(myList)
# f_url2.close()


    def isok(self,mypath:str):
        try:
            #print('isok(self,mypath:str) mypath={}:'.format(mypath))
            # thepage = urllib.request.urlopen(mypath)
            # extensions = ('.mp3', '.avi','.pdf','.jpg','.mp4')
            # exts = tuple(extensions)
            # if mypath.__contains__(exts):
            #     return False
            extensions=['.pdf','.jpg','.mp4','.exe','.mp3','.avi']
            for ext in extensions:
                if ext.lower() in mypath.lower():
                    return False

            if validators.url(mypath):
                return True
            else:
                return False
        except Exception as e:
            print(str(e))
            return False





    def process_queue(self):
        while True:
            vat,current_url, number_current_url,mongodb_urls_collection,mongodb_scraped_content_collection = self.url_queue.get()
            if self.isok(current_url):
                #logging.info('Thread: ' + str(threading.get_ident()) + ' | start grab ' + current_url)
                print('Thread: ' + str(threading.get_ident()) + ' | start grab ' + current_url)
                self.scrape_full_website(vat,current_url, SCRAPE_LEVELS,number_current_url,mongodb_urls_collection,mongodb_scraped_content_collection)
            self.url_queue.task_done()


    url_queue = Queue()


    def cleanMe(self,response):
        soup = bs4.BeautifulSoup(response.text, "html.parser")  # create a new bs4 object from the html data loaded
        for script in soup(["script", "style"]):  # remove all javascript and stylesheet code
            script.extract()
        [s.extract() for s in soup('script')]
        # get text
        text = soup.get_text(strip=False)
        # break into lines and remove leading and trailing space on each
        lines = (line.strip() + os.linesep for line in text.splitlines())
        # break multi-headlines into a line each
        chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
        # drop blank lines
        text = ' '.join(chunk for chunk in chunks if chunk)
        return text


    def scrape_specific_url(self,url_link, levels,number_of_processed):
        if not (url_link.startswith('http')):
            url_link='http://'+url_link
        if levels == 0:
            return ''
        scraped_text = []

        if self.isok(url_link):
            with self.print_lock:
                #number_of_processed = len([f for f in os.listdir(self.output_dir) if isfile(join(self.output_dir, f))])
                #logging.info(
                    # 'Thread {} | start capturing {} | level {} | No. {} from {}'.format(threading.get_ident(),
                    #                                                                     url_link, str(
                    #         SCRAPE_LEVELS + 1 - levels), number_of_processed - 1, self.number_of_input_records))
                print('Thread {} | start capturing {} | level {} | Already done {} from {}'.format(threading.get_ident(),
                                                                                          url_link, str(
                        SCRAPE_LEVELS + 1 - levels), number_of_processed, self.number_of_input_records_to_scrape))

            try:
                response = requests.get(url_link, headers=HEADERS)
                if response.history:  #was redirected
                    print('Request was redirected')
                    response = requests.get(response.url)
                    #explaining the redirect thing:
                    for resp in response.history:
                        print(resp.status_code, resp.url)
                    print('Final destination:')
                    print(response.status_code, response.url)
                soup = bs4.BeautifulSoup(response.text, "html.parser")
                scraped_text.append(self.cleanMe(response))
                self.already_scraped_list.append(url_link)
                for link in soup.find_all('a'):
                    # time.sleep(random.randint(1, 2))  # will sleep a random number of seconds
                    internal_url_link = str(link.get('href'))
                    if internal_url_link.startswith('/') and url_link.endswith('/'):
                        internal_url_link = url_link + internal_url_link.replace('/', '', 1)
                    if internal_url_link.startswith('#'):
                        internal_url_link = url_link + internal_url_link
                    scraped_text.append('\n'+internal_url_link)
                    if self.isok(internal_url_link):
                        try:
                            # response2 = requests.get(internal_url_link, headers=HEADERS)
                            # scraped_text.append(cleanMe(response2))
                            if (internal_url_link not in self.already_scraped_list and self.isok(internal_url_link)):
                                internal_scraped_result_list=self.scrape_specific_url(internal_url_link, levels - 1,number_of_processed)
                                internal_scraped_result=''.join(internal_scraped_result_list)
                                #print('print (internal_url_link not in self.already_scraped_list):\n'+internal_scraped_result)
                                scraped_text.append('\n ' + internal_scraped_result)
                        except TimeoutError:
                            scraped_text.append('\n ' + self.scrape_specific_url(internal_url_link, levels - 1,number_of_processed))
                        except Exception as e:
                            self.already_scraped_list.append(internal_url_link)
                            print('Exception 1: ' + str(e))

            except Exception as e:
                print('Exception 2: '+str(e))
                return ''

            if self.isok(url_link):
                with self.print_lock:
                    #number_of_processed = len([f for f in os.listdir(self.output_dir) if isfile(join(self.output_dir, f))])
                    #logging.info(
                        # 'Thread {} | finished capturing {} | level {} | No. {} from {}'.format(threading.get_ident(),
                        #                                                                        url_link, str(
                        #         SCRAPE_LEVELS + 1 - levels), number_of_processed - 1, self.number_of_input_records))
                    print('Thread {} | finished capturing {} | level {} | Busy on {} from {}'.format(threading.get_ident(),
                                                                                                 url_link, str(
                            SCRAPE_LEVELS + 1 - levels), number_of_processed, self.number_of_input_records_to_scrape))

            return scraped_text




    def scrape_full_website(self,vat, current_url:str, levels,number_current_url,mongodb_urls_collection,mongodb_scraped_content_collection):
        mdb=MongoDB_utilities(self.mongo_server,self.mongo_port,self.mongodb)
        cursor=mdb.get_all_companies(mongodb_scraped_content_collection,True)
        number_of_processed=cursor.count()
        cursor.close()
        if not current_url.startswith('http'):
            current_url='http://'+current_url
        url_parsed = (urllib.parse.urlparse(current_url)[1]).strip().replace('\\', '_').encode('utf-8').decode('utf-8')
        #filename = self.output_dir + str(vat) + str('.json')
        #let's first check if there is a redirect, and update the db with the new redirected url:
        response=None
        try:
            response = requests.get(current_url, headers=HEADERS)
        except (requests.exceptions.ConnectionError,urllib3.exceptions.MaxRetryError,urllib3.exceptions.NewConnectionError,socket.gaierror) as e:
            print(str(e))
            return

        if response.history:  # was redirected
            print('Request was redirected')
            response = requests.get(response.url)
            # explaining the redirect thing:
            for resp in response.history:
                print(resp.status_code, resp.url)
            print('Final destination:')
            print(response.status_code, response.url)
            mdb.update_one_field_with_vat_as_key(mongodb_urls_collection, vat, 'url', response.url)

        scraped_text = self.scrape_specific_url(response.url, levels,number_current_url)
        scrapedUrlObject = ScrapedUrl(vat, current_url, url_parsed, scraped_text)
        print('scrapedUrlObject: '+str(scrapedUrlObject.__dict__))
        try:
            self.insert_scraped_url_contents_file_in_db(json.dumps(scrapedUrlObject.__dict__),urls_collection=mongodb_urls_collection,db_scraped_content_collection=mongodb_scraped_content_collection)
        except Exception as e:
            print()
            print(str(e))
            raise e
        print('Thread {} | Inserted scrapedUrlObject {}'.format(threading.get_ident(),scrapedUrlObject.__dict__))
        # if not os.path.exists(self.output_dir):
        #     os.makedirs(self.output_dir)
        # f_result_nl = open(filename, 'w', encoding="utf-8")  # https://stackoverflow.com/a/42495690
        # # f_result_nl.write(current_url + '\n' + str(scraped_text))
        # f_result_nl.write(json.dumps(scrapedUrlObject.__dict__))  # based on https://stackoverflow.com/a/10252138/7194726
        # # f_result_nl.write(jsonpickle.encode(scrapedUrlObject)) #based on https://stackoverflow.com/a/10252138/7194726
        # f_result_nl.close()
        #number_of_processed = len([f for f in os.listdir(self.output_dir) if isfile(join(self.output_dir, f))])
        print('Thread {} | Captured {} | Already processed {} from {}'.format(threading.get_ident(), url_parsed,
                                                                              number_current_url, self.number_of_input_records_to_scrape))
        if self.app is not None:
            print_and_emit(self.app,'Thread {} | Captured {} | Already processed {} from {}'.format(threading.get_ident(), url_parsed,
                                                                                                    number_current_url, self.number_of_input_records_to_scrape), EVENT, NAMESPACE)
        #logging.info('Thread {} | Captured {} | Already processed {} from {}'.format(threading.get_ident(), url_parsed,
                                                                                     # number_current_url,
                                                                                     # self.number_of_input_records))



    def insert_scraped_url_contents_file_in_db(self,json_contents: str, urls_collection,db_scraped_content_collection):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        try:
            new_data = {}
            old_data=json.loads(json_contents)
            new_data["vat"]=old_data["vat"]
            if len(new_data["vat"]) ==0:
                new_data["vat"]=mdb.find_company_with_given_url_unparsed(old_data["url_not_parsed"],urls_collection)
            new_data["url_not_parsed"] = old_data["url_not_parsed"]
            new_data["url_parsed"] = old_data["url_parsed"]
            print('old_data["scrapedContent"]: '+str(old_data["scrapedContent"]))
            new_data["scrapedContent"] = (' '.join(old_data["scrapedContent"]))
            new_data["lastupdated"]=old_data["lastupdated"]
            # db[db_collection].insert(new_data)
            mdb.insert_item_with_vat_as_key(db_scraped_content_collection,new_data,True)
        except ValueError as e:
            print("Value Error: ", e)
            raise e


    def scrape_urls_from_vat_urls_list_to_db(self, vat_urls_list, collection_with_scraped_content, collection_with_urls,number_of_threads:int):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        cursor = mdb.get_all_companies(collection_with_scraped_content, True)
        cursor.close()
        number_of_input_records=len(vat_urls_list)
        self.number_of_input_records_to_scrape=number_of_input_records
        if self.app is not None:
            print_and_emit(self.app,'Thread: {} | We found {} urls (of which some may be processed already)'.format(threading.get_ident(), self.number_of_input_records_to_scrape), EVENT, NAMESPACE)

        for i in range(number_of_threads):
            t = threading.Thread(target=self.process_queue)
            t.daemon = True
            t.start()

        start = time.time()

        number_current_url = 0
        number_current_url_incl_skipped = 0
        for vat,url in vat_urls_list:
            number_current_url_incl_skipped+=1
            try:
                if len(url) > 0:
                    url_parsed = (urllib.parse.urlparse(url)[1]).strip().replace('\\', '_').encode('utf-8').decode('utf-8')
                    document=mdb.find_scraped_content_with_given_url_unparsed(url, collection_with_scraped_content)
                    try:
                        scraped_content=document['scrapedContent']
                    except:
                        scraped_content=''

                    if len(scraped_content.strip())>0:
                        print('Thread {} | vat {} | current scraped_content {}'.format(threading.get_ident(),vat,scraped_content))
                        self.init_number_of_processed+=1
                        print('Thread: {} | Skipping no {} of {} since exists already {}'.format(threading.get_ident(),
                                                                                                 number_current_url_incl_skipped,
                                                                                                 number_of_input_records,
                                                                                                 url_parsed))
                        self.number_of_input_records_to_scrape-=1
                        # logging.info('Thread: {} | Skipping since exists already {}'.format(threading.get_ident(),number_current_url,self.number_of_input_records, url_parsed))
                        continue
                    number_current_url += 1
                    self.url_queue.put((vat, url, number_current_url, collection_with_urls, collection_with_scraped_content))
            except Exception as e:
                self.number_of_input_records_to_scrape -= 1
                if self.app is not None:
                    print_and_emit(self.app, 'Thread: {} | Skipping since exception occured {} for company_url: '.format(threading.get_ident(), str(e), url), EVENT, NAMESPACE)
                raise e
        self.url_queue.join()

        print(threading.enumerate())

        print("Execution time = {0:.5f}".format(time.time() - start))




    def scrape_with_db_if_url_field_exists(self, collection_with_urls,collection_with_scraped_content):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)
        cursor=mdb.get_all_companies_with_existing_field(collection_with_urls,field_name="url",must_exist=True,timeout=False)
        self.number_of_input_records_to_scrape=cursor.count()
        self.init_number_of_processed =0 #this we use for the caluclations of already scraped records in the log and should not be change
        if self.app is not None:
            print_and_emit(self.app,'Thread: {} | We found {} urls (of which some may be processed already)'.format(threading.get_ident(), self.number_of_input_records_to_scrape), EVENT, NAMESPACE)

        #logging.info('starting scrape with ' + str(THREADS) + ' threads and ' + str(SCRAPE_LEVELS) + ' depth levels')

        for i in range(THREADS):
            t = threading.Thread(target=self.process_queue)
            t.daemon = True
            t.start()

        start = time.time()


        number_current_url=0
        for document in cursor:
            url_parsed=''
            try:
                vat=document['vat']
                current_url = document['url']
                if len(current_url) > 0:
                    url_parsed = (urllib.parse.urlparse(current_url)[1]).strip().replace('\\', '_').encode('utf-8').decode('utf-8')
                    # print(url_parsed)
                    #filename = self.output_dir + str(vat) + str('.json')
                    #if file_exists(filename): #TODO reinstate the following or find_company(vat,SCRAPED_CONTENT) is not None:
                    if mdb.find_company(vat,collection_with_scraped_content) is not None:
                        print('Thread: {} | Skipping no {} of {} since exists already {}'.format(threading.get_ident(), number_current_url, self.number_of_input_records_to_scrape, url_parsed))
                        #logging.info('Thread: {} | Skipping since exists already {}'.format(threading.get_ident(),number_current_url,self.number_of_input_records, url_parsed))
                        continue
                    number_current_url += 1
                    self.url_queue.put((vat, current_url, number_current_url,collection_with_urls,collection_with_scraped_content))
            except OSError as e:
                if self.app is not None:
                    print_and_emit(self.app,'Thread: {} | Skipping since exception occured {} for company_url: '.format(threading.get_ident(), str(e),url_parsed),EVENT,NAMESPACE)

        self.url_queue.join()
        cursor.close()

        print(threading.enumerate())

        print("Execution time = {0:.5f}".format(time.time() - start))


    #based on http://scraping.pro/simple-email-crawler-python/
    def scrape_only_email_from_scraped_content_in_db_or_scrape_first(self, vat, input_url, collection_urls,collection_scraped_content):
        mdb = MongoDB_utilities(self.mongo_server, self.mongo_port, self.mongodb)

        # a queue of urls to be crawled
        new_urls = deque([input_url])

        # a set of urls that we have already crawled
        processed_urls = set()

        # a set of crawled emails
        emails = set()

        # process urls one by one until we exhaust the queue
        number_current_url=0
        while len(new_urls):
            number_current_url+=1
            # move next url from the queue to the set of processed urls
            url = new_urls.popleft()
            processed_urls.add(url)

            document = mdb.find_company(vat, collection_scraped_content)
            if document==None:
                # let's scrape the website and add to db, not always the email is on the first page
                self.scrape_full_website(vat, url, SCRAPE_LEVELS, number_current_url, collection_urls,
                                         collection_scraped_content)
                document = mdb.find_company(vat,collection_scraped_content)
                if document==None:  #for example if the website was not found, is a dead link etc
                    return ''
            try:
                already_scraped_url_content=document['scrapedContent']
            except Exception as e:
                print('An exception has ocured: {}'.format(str(e)))
                raise e
            # extract all email addresses and add them into the resulting set
            new_emails = set(re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", already_scraped_url_content, re.I))
            if (len(new_emails)>0):
                print(new_emails)
                emails.update(new_emails)
                continue


            # extract base url to resolve relative links
            parts = urlsplit(url)
            base_url = "{0.scheme}://{0.netloc}".format(parts)
            path = url[:url.rfind('/') + 1] if '/' in parts.path else url

            # get url's content
            print("Processing %s" % url)
            try:
                response = requests.get(url)
                if response.history:  #was redirected
                    print('Request was redirected')
                    response = requests.get(response.url)
                    #explaining the redirect thing:
                    for resp in response.history:
                        print(resp.status_code, resp.url)
                    print('Final destination:')
                    print(response.status_code, response.url)
                    mdb.update_one_field_with_vat_as_key(collection_urls, vat, 'url', response.url)

                    #>>> import requests
                    # >>> response = requests.get('http://httpbin.org/redirect/3')
                    # >>> response.history
                    # (<Response [302]>, <Response [302]>, <Response [302]>)
                    # >>> for resp in response.history:
                    # ...     print resp.status_code, resp.url
                    # ...
                    # 302 http://httpbin.org/redirect/3
                    # 302 http://httpbin.org/redirect/2
                    # 302 http://httpbin.org/redirect/1
                    # >>> print response.status_code, response.url
                    # 200 http://httpbin.org/get


            except (requests.exceptions.MissingSchema, requests.exceptions.ConnectionError):
                # ignore pages with errors
                continue

            # extract all email addresses and add them into the resulting set
            new_emails = set(re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", response.text, re.I))
            emails.update(new_emails)

            # # create a beutiful soup for the html document
            # soup = bs4.BeautifulSoup(response.text)
            #
            # # find and process all the anchors in the document
            # for anchor in soup.find_all("a"):
            #     # extract link url from the anchor
            #     link = anchor.attrs["href"] if "href" in anchor.attrs else ''
            #     # resolve relative links
            #     if link.startswith('/'):
            #         link = base_url + link
            #     elif not link.startswith('http'):
            #         link = path + link
            #     # add the new url to the queue if it was not enqueued nor processed yet
            #     if not link in new_urls and not link in processed_urls:
            #         new_urls.append(link)
        return emails


    #based on http://edmundmartin.com/scraping-google-with-python/
    #and https://vverma.net/scrape-the-web-using-css-selectors-in-python.html
    #dit zou moeten lukken:
    #https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=VOF%20EETERIJ%20EN%20IJSKAFFEE%20HET%20AMBACHT%204553AV%20HAVENSTRAAT&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry&key=AIzaSyAUIBBsPE7OVuv6VnMx9ESxaEDnmUQANuw

    #deze (eerst placeid vinden, vervolgens websit eproberen op t vragen):
    #https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=VOF%20EETERIJ%20EN%20IJSKAFFEE%20HET%20AMBACHT%204553AV%20HAVENSTRAAT&inputtype=textquery&key=AIzaSyAUIBBsPE7OVuv6VnMx9ESxaEDnmUQANuw
    #https://maps.googleapis.com/maps/api/place/details/json?placeid=31214639d5a7b55a7ebab01ce1aafb723d34480c=website&key=AIzaSyAUIBBsPE7OVuv6VnMx9ESxaEDnmUQANuw
    def scrape_google_places_json_api(self,search_term):
        google_api_key='AIzaSyBDeeMkUlNi7Lz4kIl1zOSwBmhAerc8IFk'
        escaped_search_term = search_term.replace(' ', '+')
        basic_google_api_url='https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input={}&inputtype=textquery&key={}'.format(escaped_search_term,google_api_key)
        print('basic_google_api_url: '+basic_google_api_url)
        google_basic_places_search = requests.get(basic_google_api_url)
        google_basic_places_search_json = google_basic_places_search.json()

        try:
            place_id=google_basic_places_search_json['candidates'][0]['place_id']
            details_google_api_url='https://maps.googleapis.com/maps/api/place/details/json?placeid={}&fields=website&key={}'.format(place_id,google_api_key)
            print(details_google_api_url)
            google_details_places_search = requests.get(details_google_api_url)
            google_details_places_search_json = google_details_places_search.json()
            return google_details_places_search_json['result']['website']
        except:
            return ''

    def scrape_online_search_engine_with_keyword_return_first_url(self, search_term, number_results, language_code):
        url_domain_list_to_skip=['knack.be','companyweb.be','staatsbladmonitor.be','goudengids.be','companycheck.co.uk']
        assert isinstance(search_term, str), 'Search term must be a string'
        assert isinstance(number_results, int), 'Number of results must be an integer'
        escaped_search_term = search_term.replace(' ', '+')

        google_url = 'https://www.google.com/search?q={}&num={}&hl={}'.format(escaped_search_term, number_results,
                                                                              language_code)

        yahoo_url='https://nl.search.yahoo.com/search?p={}&fr=yfp-t&fp=1&toggle=1&cop=mss&ei=UTF-8'.format(escaped_search_term)
        try:
            response, results = self.collect_search_engine_results(google_url,css_selector='.r a')
            if len(results)==0:
                response, results = self.collect_search_engine_results(yahoo_url,css_selector='#web .lh-17')
            #print(response.text)

            for match in results:
                # get the href attribute of the result
                #print(match.get('href'))
                url_link = str(match.get('href'))

                if len(url_link)==0:

                    url_link=match
                parsed_uri = urlparse(url_link)
                url_link_domain = '{uri.netloc}'.format(uri=parsed_uri)# see https://docs.python.org/3/library/urllib.parse.html
                if len(url_link_domain)==0:
                    url_link_domain='{uri.path}'.format(uri=parsed_uri)
                url_link_domain=url_link_domain.split(".")[-2:]
                url_link_domain='.'.join(url_link_domain)
                print('url_link_domain: '+url_link_domain)
                if (url_link_domain in url_domain_list_to_skip):
                    continue

                if self.isok(url_link):
                    try:
                        return url_link
                    except TimeoutError:
                        return self.scrape_online_search_engine_with_keyword_return_first_url(search_term, number_results, language_code)
                    except:
                        pass
        except Exception as e:
            print(str(e))
            #pass

        return ''


    def collect_search_engine_results(self, search_engine_url, css_selector:str):
        response = requests.get(search_engine_url, headers=HEADERS)
        # build the DOM Tree
        tree = lxml.html.fromstring(response.text)
        # construct a CSS Selector
        sel = CSSSelector(css_selector)
        # Apply the selector to the DOM tree.
        results = sel(tree)
        return response, results

    # def scrape_only_belg_phone_numbers_with_db_if_url_field_exists(self, input_url):
    #     # a queue of urls to be crawled
    #     new_urls = deque([input_url])
    #
    #     # a set of urls that we have already crawled
    #     processed_urls = set()
    #
    #     # a set of crawled phone numbers
    #     mobilephones = set()
    #     landlines=set()
    #
    #     # process urls one by one until we exhaust the queue
    #     while len(new_urls):
    #
    #         # move next url from the queue to the set of processed urls
    #         url = new_urls.popleft()
    #         processed_urls.add(url)
    #
    #         # extract base url to resolve relative links
    #         parts = urlsplit(url)
    #         base_url = "{0.scheme}://{0.netloc}".format(parts)
    #         path = url[:url.rfind('/') + 1] if '/' in parts.path else url
    #
    #         # get url's content
    #         print("Processing %s" % url)
    #         try:
    #             response = requests.get(url)
    #         except (requests.exceptions.MissingSchema, requests.exceptions.ConnectionError):
    #             # ignore pages with errors
    #             continue
    #
    #         # extract all email addresses and add them into the resulting set
    #         # Belgian phone number
    #         # /^((\+|00)32\s?|0)(\d\s?\d{3}|\d{2}\s?\d{2})(\s?\d{2}){2}$/
    #         # Belgian mobile phone number
    #         # /^((\+|00)32\s?|0)4(60|[789]\d)(\s?\d{2}){3}$/
    #         new_landlines = set(re.findall(r"/^((\+|00)32\s?|0)(\d\s?\d{3}|\d{2}\s?\d{2})(\s?\d{2}){2}$/", response.text, re.I))
    #         landlines.update(new_landlines)
    #         new_mobile_phones=set(re.findall(r"/^((\+|00)32\s?|0)4(60|[789]\d)(\s?\d{2}){3}$/", response.text, re.I))
    #         mobilephones.update(new_mobile_phones)
    #
    #         # # create a beutiful soup for the html document
    #         # soup = bs4.BeautifulSoup(response.text)
    #         #
    #         # # find and process all the anchors in the document
    #         # for anchor in soup.find_all("a"):
    #         #     # extract link url from the anchor
    #         #     link = anchor.attrs["href"] if "href" in anchor.attrs else ''
    #         #     # resolve relative links
    #         #     if link.startswith('/'):
    #         #         link = base_url + link
    #         #     elif not link.startswith('http'):
    #         #         link = path + link
    #         #     # add the new url to the queue if it was not enqueued nor processed yet
    #         #     if not link in new_urls and not link in processed_urls:
    #         #         new_urls.append(link)
    #     return emails
