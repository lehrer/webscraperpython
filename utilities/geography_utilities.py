import threading

import requests
import json
import geopy.distance


from utilities.utilities import log_and_print

#OS_PHOTON_BASE_URL='http://photon.komoot.de/api/'
OS_PHOTON_BASE_URL="http://localhost:2322/api"
LOCATIONIQ_BASE_URL='https://us1.locationiq.org/v1/search.php'
LOCATIONIQ_TOKEN='90e6efe91e9e35'
LOCATION_API_KEY='pk.53fc0902743711fbb3f69ae614cbd2a9'

def get_lat_long_photon( address):
    query='?q='+address
    url= OS_PHOTON_BASE_URL + query +'&limit=1'
    log_and_print('Thread: {} | url: {}'.format(threading.get_ident(),url))

    try:
        places = requests.get(url)
        places_json = places.json()
    except json.decoder.JSONDecodeError as e:
        log_and_print('Thread: {} | exception occured for requests.get(url) or  places.json() -> vat: {} / url: {} : exception: {}'.format(threading.get_ident(),address,url,str(e)))
        return None
    log_and_print('Thread: {} | for URL: {} we received as json response from Photon: {}'.format(threading.get_ident(),url,places.text))


    #log_and_print('Thread: {} | json response from Photon received: {}'.format(threading.get_ident,places_json))
    try:
        long=str(places_json['features'][0].get('geometry').get('coordinates')[0])
        lat=str(places_json['features'][0].get('geometry').get('coordinates')[1])
        log_and_print('Thread: {} | found lat {} and long {} (file: {})'.format(threading.get_ident(),lat,long, places_json))
        return '{},{}'.format(lat, long)
    except (IndexError) as e:
        log_and_print('Thread: {} | IndexError ({}) (file: {})'.format(threading.get_ident(), str(e),places_json))
        return None

def get_lat_long_locationiq(address):
    query=address.encode('utf-8').decode('utf-8')
    data = {
        'key': LOCATIONIQ_TOKEN,
        'q': query,
        'format': 'json'
    }
    try:
        places = requests.get(LOCATIONIQ_BASE_URL,params=data)
        places_json = places.json()
        print('places_json: '+str(places_json))

        for item in places_json:
        # print(item)
            print('lat: ' + item['lat'])
            print('lon: ' + item['lon'])
            print('display_name: ' + item['display_name'])
            if item['lat'] is not None and item['lon'] is not None:
                return '{},{}'.format(item['lat'],item['lon'])
            else:
                return None
    except:
        return None

def get_lat_long_all_methods(address):
    lat_long_string=None
    lat_long_string = get_lat_long_photon((address + ', ' + 'Belgium').encode('utf-8').decode('utf-8').replace('&', ''))
    if lat_long_string is None:  # photon did not work, we try again photon without the country:
        lat_long_string = get_lat_long_photon(address.encode('utf-8').decode('utf-8').replace('&', ''))
    if lat_long_string is None:  # photon did not work at all, we try with locationiq
        lat_long_string = get_lat_long_locationiq((address + ', ' + 'Belgium').encode('utf-8').decode('utf-8').replace('&', ''))
    return lat_long_string

#examples:
# coords1=(52.2296756, 21.0122287)
# coords2=(52.406374, 16.9251681)
# #print(calcuclate_distance_between_given_coords(coords1,coords2))
# print(find_coordinates_within_given_distance_meters(coords1,1000))
# print(find_coordinates_within_given_distance_meters(coords1,400))
def calcuclate_distance_between_given_coords(coords_1:tuple,coords_2:tuple):
    return geopy.distance.vincenty(coords_1, coords_2).m


#based on https://stackoverflow.com/a/24429798/7194726
def find_coordinates_within_given_distance_meters(self, coords, distance_in_meters):
    # Define starting point.
    start = geopy.Point(coords)

    # Define a general distance object, initialized with a distance of 1 km.
    d = geopy.distance.VincentyDistance(meters=distance_in_meters) #options: meters, kilometers (check for possibly more)

    # Use the `destination` method with a bearing of 0 degrees (which is north)
    # in order to go from point `start` 1 km to north.
    point:geopy.point.Point=d.destination(point=start, bearing=0)
    lat=point.latitude
    long=point.longitude
    return('lat: {}, long:{}'.format(lat,long))


