import csv
import fnmatch
import logging
import logging.handlers
import os
import threading
from pathlib import Path

from flask import Flask
from flask_socketio import emit



def getfiles(path):
    if len(path) <= 1:
        print('!Please Supply an Input File')
        return []
    try:
        input_path = str(path).strip()

        if os.path.exists(input_path) == 0:
            print('!Input Path does not exist (input_path = ', input_path, ')')
            return []

        if os.path.isdir(input_path) == 0:
            print('*Input Path is Valid (input_path = ', input_path, ')')
            return [input_path]

        matches = []
        for root, dirnames, filenames in os.walk(input_path):
            for filename in fnmatch.filter(filenames, '*.json'):
                matches.append(os.path.join(root, filename))

        if len(matches) > 0:
            print('*Found Files in Path (input_path = ', input_path, ', total-files = ', len(matches), ')')
            return matches

        print('!No Files Found in Path (input_path = ', input_path, ')')
    except ValueError:
        print('!Invalid Input (input_path, ', input_path, ')')
    return []

def file_exists(path):
    my_file = Path(path)
    if my_file.is_file():
        return True
    else:
        return False



def log_and_print(outputtext):
    logging.info(outputtext)
    print(outputtext)


def init_logger(logfile_path,file_name, name):
    # logging (based on https://stackoverflow.com/a/15167862/7194726)
    if not os.path.exists(logfile_path):
        os.makedirs(logfile_path)
    my_logger = logging.getLogger(name)
    my_logger.setLevel(logging.DEBUG)

    # Add the log message handler to the logger
    handler = logging.handlers.RotatingFileHandler(
        logfile_path + file_name, maxBytes=20, backupCount=5)
    formatter = logging.Formatter('%(asctime)s %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p')
    handler.setFormatter(formatter)

    my_logger.addHandler(handler)

def save_list_to_csv_file(dir,filename, header_list, list_to_save:list,mode):
    fullpath=dir+filename
    if not os.path.exists(dir):
        os.makedirs(dir)
    f_result_nl = open(fullpath, mode=mode,
                       encoding="utf-8")  # https://stackoverflow.com/a/42495690
    f_result_nl.write(header_list+'\n')  # based on https://stackoverflow.com/a/10252138/7194726
    # f_result_nl.write(jsonpickle.encode(scraped_company)) #based on https://stackoverflow.com/a/10252138/7194726
    for item in list_to_save:
        #f_result_nl.write(item.to_csv(';')+'\n')
        f_result_nl.write(item + '\n')
    f_result_nl.close()

def append_text_to_text_file(fullpath, string_to_append:str, mode):
    dir=os.path.dirname(os.path.abspath(fullpath))  #based on https://stackoverflow.com/q/17057544/7194726
    if not os.path.exists(dir):
        os.makedirs(dir)
    f_result_nl = open(fullpath, mode=mode,
                       encoding="utf-8")  # https://stackoverflow.com/a/42495690
    f_result_nl.write(string_to_append)  # based on https://stackoverflow.com/a/10252138/7194726
    # f_result_nl.write(jsonpickle.encode(scraped_company)) #based on https://stackoverflow.com/a/10252138/7194726
    f_result_nl.close()

def get_vat_array_from_csv_file(input_csv_with_vats,vat_column_id:int):
    values_to_be_skipped=['NULL']
    print('Thread {} | collecting vats'.format(threading.get_ident()))
    vats = set()
    csvreader = csv.reader(input_csv_with_vats, delimiter=';')
    current_row = 0
    row_count = sum(1 for line in open(input_csv_with_vats, encoding="utf-8"))
    with open(input_csv_with_vats, 'r', encoding='utf-8') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=';')
        for row in csvreader:
            current_row += 1
            print('Thread {} | currently working on row {} of {}'.format(threading.get_ident(),current_row,row_count))
            if current_row == 1:
                continue  # the first and second row is a row with headers
            vat = row[vat_column_id]
            if len(vat) == 0:
                print('Thread {} | no vat found for row {} of {}'.format(threading.get_ident(), current_row, row_count))
                continue
            if vat in vats:
                print('Thread {} | we saw that vat ({}) already for row {} of {}'.format(threading.get_ident(), vat,
                                                                      current_row, row_count))
                continue
            if vat in values_to_be_skipped:
                print('Thread {} | this vat-value should be skipped ({}) for row {} of {}'.format(threading.get_ident(), vat,
                                                                                         current_row, row_count))
                continue
            if not vat.startswith('BE'):
                vat=vat.replace('.', '')
                while(len(vat)<10):
                   vat=str(0)+vat
                vat = 'BE' + vat
            vats.add(vat)
        return list(vats)

def get_vat__source_name_array_from_csv_file(input_makro_csv,vat_column_id:int,source_name_column_id:int):
    values_to_be_skipped=['NULL']
    print('Thread {} | collecting vats'.format(threading.get_ident()))
    vats_names = set()
    vats=set()
    csvreader = csv.reader(input_makro_csv, delimiter=';')
    current_row = 0
    row_count = sum(1 for line in open(input_makro_csv, encoding="utf-8"))
    with open(input_makro_csv, 'r', encoding='utf-8') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=';')
        for row in csvreader:
            current_row += 1
            print('Thread {} | currently working on row {} of {}'.format(threading.get_ident(),current_row,row_count))
            if current_row == 1:
                continue  # the first and second row is a row with headers
            vat = row[vat_column_id]
            name=row[source_name_column_id]
            if len(vat) == 0:
                print('Thread {} | no vat found for row {} of {}'.format(threading.get_ident(), current_row, row_count))
                continue
            if vat in vats:
                print('Thread {} | we saw that vat ({}) already for row {} of {}'.format(threading.get_ident(), vat,
                                                                      current_row, row_count))
                continue
            if vat in values_to_be_skipped:
                print('Thread {} | this vat-value should be skipped ({}) for row {} of {}'.format(threading.get_ident(), vat,
                                                                                         current_row, row_count))
                continue
            if not vat.startswith('BE'):
                vat=vat.replace('.', '')
                while(len(vat)<10):
                   vat=str(0)+vat
                vat = 'BE' + vat
            vats.add(vat)
            vats_names.add((vat,name))
        return list(vats_names)

def get_vat_url_list_from_csv_file(input_csv_file, vat_column_id:int, url_column_id:int):
    values_to_be_skipped=['NULL']
    print('Thread {} | collecting vats'.format(threading.get_ident()))
    vats_urls = set()
    vats=set()
    csvreader = csv.reader(input_csv_file, delimiter=';')
    current_row = 0
    row_count = sum(1 for line in open(input_csv_file, encoding="utf-8"))
    with open(input_csv_file, 'r', encoding='utf-8') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=';')
        for row in csvreader:
            current_row += 1
            print('Thread {} | currently working on row {} of {}'.format(threading.get_ident(),current_row,row_count))
            if current_row == 1:
                continue  # the first and second row is a row with headers
            vat = row[vat_column_id]
            url=row[url_column_id]
            if len(vat) == 0:
                print('Thread {} | no vat found for row {} of {}'.format(threading.get_ident(), current_row, row_count))
                continue
            if vat in vats:
                print('Thread {} | we saw that vat ({}) already for row {} of {}'.format(threading.get_ident(), vat,
                                                                      current_row, row_count))
                continue
            if vat in values_to_be_skipped:
                print('Thread {} | this vat-value should be skipped ({}) for row {} of {}'.format(threading.get_ident(), vat,
                                                                                         current_row, row_count))
                continue
            if not vat.startswith('BE'):
                vat=vat.replace('.', '')
                while(len(vat)<10):
                   vat=str(0)+vat
                vat = 'BE' + vat
            vats.add(vat)
            vats_urls.add((vat,url))
        return list(vats_urls)


def get_column_value_from_csv_file(input_csv_with_values,column_id:int,seperator:str):
    print('Thread {} | collecting column'.format(threading.get_ident()))
    values = []
    csvreader=None
    #csvreader = csv.reader(input_csv_with_values, delimiter=';')
    current_row = 0
    row_count = sum(1 for line in open(input_csv_with_values, encoding="latin-1"))
    with open(input_csv_with_values, 'r', encoding='latin-1') as csvfile:
        if seperator==None:
            csvreader = csv.reader(csvfile)
        else:
            csvreader = csv.reader(csvfile, delimiter=seperator)
        for row in csvreader:
            current_row += 1
            print('Thread {} | currently working on row {} of {}'.format(threading.get_ident(),current_row,row_count))
            if current_row == 1:
                continue  # the first and second row is a row with headers
            if seperator==None:
                value = ''.join(row).upper()
            else:
                value = row[column_id].upper()
            if len(value) == 0:
                print('Thread {} | no value found for row {} of {}'.format(threading.get_ident(), current_row, row_count))
                continue
            if value in values:
                print('Thread {} | we saw that value ({}) already for row {} of {}'.format(threading.get_ident(), value,
                                                                                         current_row, row_count))
                continue
            values.append(value)
        return values


def print_and_emit(app:Flask,text_to_print:str, event:str, namespace:str):
    print(text_to_print)
    with app.app_context():
        emit(event,text_to_print,namespace=namespace,broadcast=True)

def read_config_file_return_configs_list(fullpath):
    config_dict = {}
    with open(fullpath) as f:
        for line in f.readlines():
            value = line.strip()
            try:
                if (value.__contains__('=')):
                    print('value: '+ value)
                    config_dict[value.split('=')[0]]=value.split('=')[1]
                    print(config_dict)
            except:
                pass
        return config_dict