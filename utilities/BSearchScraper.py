import datetime
import threading
import logging
import os
import bs4
import requests
import validators
import json

from easy_order.MongoDB_Utilities import MongoDB_utilities
from models.Company import Company

print_lock = threading.Lock()

# constants:
VAT_COLUMN_INDEX=1
CSV_DELIMITER_CHAR=','
INPUT_CSV_FILE = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\ReceivedRawLists\\annual_accounts_2015_2016.csv'
#BASE_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\Get BsearchInfo\\'
TIMESTAMP = datetime.datetime.now().isoformat().replace(':', '_')
OUTPUT_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\Companies\\annual_accounts_2015_2016\\companies_full\\'
LOGS_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\logs\\'
HEADERS = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
}
BSEARCH_BASE_URL = 'https://www.bsearch.be/'


class BSearchScraper():


    def __init__(self,mongodb_server,mongodb_port,mongodb_db):
        self.mdb = MongoDB_utilities(mongodb_server, mongodb_port, mongodb_db)

        # logging (based on https://stackoverflow.com/a/15167862/7194726)
        if not os.path.exists(LOGS_DIR):
            os.makedirs(LOGS_DIR)
        logging.getLogger('').handlers = []
        logging.basicConfig(
            filename=LOGS_DIR + 'scraper_' + TIMESTAMP + '.log',
            format='%(asctime)s %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
            filemode="w",
            level=logging.DEBUG)

        # variables

    def isok(self,mypath):
        try:
            # thepage = urllib.request.urlopen(mypath)
            if mypath.endswith('.pdf'):
                return False
            if validators.url(mypath):
                return True
            else:
                return False
        except:
            # print(mypath , ' : error')
            return False

    def scrape_vat(self,current_vat,collection_db_with_urls):
        vat_url = BSEARCH_BASE_URL + current_vat
        name = ''
        address = ''
        phone = ''
        url = ''
        if self.isok(vat_url):

            try:
                response = requests.get(vat_url, headers=HEADERS)
                soup = bs4.BeautifulSoup(response.text, "html.parser")
                for n in soup.select('.name'):
                    name = n.text
                for a in soup.select('.address'):
                    address = a.text
                for p in soup.select('.read-more-wrap'):
                    phone = p.text
                for u in soup.select('.web a'):
                    url = u.attrs['href']

            except TimeoutError:
                return self.scrape_vat(current_vat,collection_db_with_urls)
            except:
                pass

            url_data = {}
            url_data["vat"] = current_vat
            url_data["url"] = url
            url_data["address"] = address
            url_data["phone"]=phone
            url_data["name"]=name
            url_data["lastupdated"]=str(datetime.datetime.now())
            if len(url)==0:
                return None
            result = None
            while result is None:
                result = self.mdb.insert_item_with_vat_as_key(collection_db_with_urls,
                                                                         url_data, True)
            logging.info(
                'Thread: {} | scraped_company = {}'.format(str(threading.get_ident()), url_data))
            print('Thread: {} | scraped_company = {}'.format(str(threading.get_ident()), url_data))
            return result


    def scrape_bsearch_with_vat_array(self, array_with_vats:list, destination_collection_with_found_urls):
        current_item_count=0
        total_items_found=len(array_with_vats)
        not_scraped_already_scraped:int=0
        not_scraped_no_url_found:int=0
        scraped:int=0

        for vat in array_with_vats:
            current_item_count += 1
            if self.mdb.find_company(vat, destination_collection_with_found_urls) is not None:
                not_scraped_already_scraped+=1
                print(
                    'bsearch info not added since already added | no. {} of {} | vat {} | data: None added'.format(
                        current_item_count,
                        total_items_found,
                        vat))
                continue
            result=self.scrape_vat(vat,destination_collection_with_found_urls)
            if result==None:
                not_scraped_no_url_found+=1
                print(
                    'bsearch info not added since no url found | no. {} of {} | vat {} | data: None added'.format(
                        current_item_count,
                        total_items_found,
                        vat))
                continue
            scraped+=1
            print(
                'bsearch info processed | no. {} of {} | vat {} | data: {}'.format(
                    current_item_count,
                    total_items_found,
                    vat,
                result))
            continue
        print('BSearch scraping done: not_scraped_already_scraped: {}\nnot_scraped_no_url_found: {}\nscraped {}'.format(not_scraped_already_scraped,not_scraped_no_url_found,scraped))
