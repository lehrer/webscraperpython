import re

from flask import Flask

from easy_order.MongoDB_Utilities import MongoDB_utilities
from utilities import utilities
import datetime
import threading

from utilities.utilities import print_and_emit

TIMESTAMP=datetime.datetime.now().isoformat().replace(':', '_')
EVENT='message'
NAMESPACE='/'

class DTM_Creator():


    def __init__(self,app:Flask,mongodb_server,mongodb_port,mongodb_db):
        self.app=app
        self.mdb = MongoDB_utilities(mongodb_server, mongodb_port, mongodb_db)

    def find_specific(self,collection_scraped_content,keyword,optional_vat_array):
        dict = {}
        regx = re.compile(keyword, re.IGNORECASE)
        if optional_vat_array ==None:
            cursor=self.mdb.find_scraped_content(keyword=keyword,mongodb_collection_scraped_content=collection_scraped_content,timeout=True)
        else:
            cursor=self.mdb.find_scraped_content_with_given_vat_array(keyword=keyword,mongodb_collection_scraped_content=collection_scraped_content,vat_array=optional_vat_array,timeout=True)
        total_row_count=cursor.count()
        print_and_emit(self.app,'Thread {} | Found keyword {} in {} urls'.format(threading.get_ident(), keyword, total_row_count), EVENT, NAMESPACE)
        current_row_no=0
        for document in cursor:
            current_row_no+=1
            number_of_occurences = len(re.findall(regx, document["scrapedContent"]))
            self.print_and_donot_emit('Thread {} | url no. {} of {}: found the keyword {} {} times for url {}"'
                                      .format(threading.get_ident(),current_row_no,total_row_count,keyword,str(number_of_occurences), str(
                document["url_not_parsed"])))
            dict[document["url_not_parsed"]] = (str(number_of_occurences))
        cursor.close()
        return dict

    def create(self,output_csv_file,collection_scraped_content,keywords_list:list,optional_vat_array):
        #keywords_list=[x.lower() for x in keywords_list] #make it lowercase #not required, therefore commenting
        dtm_dict = {}
        url_set = set()
        for keyword in keywords_list:
            dtm_dict[keyword] = self.find_specific(collection_scraped_content=collection_scraped_content,keyword=keyword.lower(),optional_vat_array=optional_vat_array)
        total_keywords_to_process=len(keywords_list)
        current_keyword_no=0
        print_and_emit(self.app,'Thread {} | we need to process {} keywords'.format(threading.get_ident(), total_keywords_to_process), EVENT, NAMESPACE)
        for k in dtm_dict:
            current_keyword_no+=1
            print_and_emit(self.app,'Thread {} | starting on keyword no {} ("{}") of {}'.format(threading.get_ident(), current_keyword_no, k, total_keywords_to_process), EVENT, NAMESPACE)

            current_url_no_to_process_per_keyword = 0
            total_urls_to_process_per_current_keyword = len(dtm_dict[k])
            for k1 in dtm_dict[k]:
                current_url_no_to_process_per_keyword+=1
                self.print_and_donot_emit('Thread {} | processing keyword: {} | url: {} | no {} of {}'.format(threading.get_ident(), k, k1, current_url_no_to_process_per_keyword, total_urls_to_process_per_current_keyword))
                url_set.add(k1)  # add all urls in a set (so only unique values are in it

        headers = 'url;'
        rows_dict = {}

        # prepare columnheader:
        for keyword in keywords_list:
            # add keyword to headers
            headers += keyword + ';'

        # prepare each row
        for url in url_set:
            # first init all rows (we don't want Null errors)
            if url not in rows_dict.keys():
                rows_dict[url] = ''
            for keyword in keywords_list:
                # if there is no value, give it value '0'
                if url not in dtm_dict[keyword].keys():
                    dtm_dict[keyword][url] = '0'
                # prepare each row
                rows_dict[url] = rows_dict[url] + str(dtm_dict[keyword][url]) + ';'

        output = headers + '\n'
        utilities.append_text_to_text_file(output_csv_file, headers + '\n', 'a')
        total_lines_to_add=len(url_set)
        current_line_no=0
        print_and_emit(self.app,'Thread {} | starting to process  {} lines'.format(threading.get_ident(),total_lines_to_add), EVENT, NAMESPACE)

        for url in url_set:
            current_line_no+=1
            line= '{};{}\n'.format(url, rows_dict[url])
            utilities.append_text_to_text_file(output_csv_file, line, 'a')
            self.print_and_donot_emit('Thread {} | added line no {} of {} | line: {}'.format(threading.get_ident(), current_line_no, total_lines_to_add, line))
            if current_line_no % 500 == 0:
                print_and_emit(self.app,
                    'Thread {} | added line no {} of {} | line: {}'.format(threading.get_ident(), current_line_no,
                                                                           total_lines_to_add, line),EVENT,NAMESPACE)

    def print_and_donot_emit(self, text_to_print:str):
        print(text_to_print)




