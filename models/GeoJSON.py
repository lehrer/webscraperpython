#see https://docs.mongodb.com/manual/reference/geojson/
from array import array


class geo_json:
    def __init__(self, type:str, lat:float, long:float):
        self.type = type
        self.coordinates=[long,lat]

    def to_dict(self):
        dict_to_return=dict()
        dict_to_return['type']=self.type.strip()
        dict_to_return['coordinates']=self.coordinates
        return(dict_to_return)

