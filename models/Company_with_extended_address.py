import uuid


class Company_with_extended_address:
    def __init__(self, id, vat, name, b_search_address,kbo_adress, phone,url, lat, long, fcbk_dict, lastupdated):
        self.id = id
        self.vat=vat
        self.name=name
        self.b_search_address = b_search_address
        self.kbo_adress = kbo_adress
        self.phone = phone
        self.url=url
        self.lat=lat
        self.long=long
        self.fcbk_dict=fcbk_dict
        self.lastupdated=lastupdated

    def __str__(self):
        return 'vat: '+str(self.vat) + ', url: '+ str(self.url)+ ', name: '+ str(self.name) + ', phone: '+ str(self.phone)