import uuid

from models.GeoJSON import geo_json
from utilities.utilities import log_and_print


class School:

    def __init__(self, net, name, street, houseno, postcode, city, phone, email, url, lat:float, long:float, lastupdated):
        self.id = str(uuid.uuid4())
        self.net = net
        self.name = name
        self.street = street
        self.houseno = houseno
        self.postcode = postcode
        self.city = city
        self.phone = phone
        self.email = email
        self.url = url
        self.lat = lat
        self.long=long
        self.lastupdated = lastupdated

    def __str__(self):
        try:
            sep=';'
            string_to_return = self.net.strip() + sep + self.name.strip() + sep + self.street.strip() + sep + self.houseno.strip() + sep + self.postcode.strip() + sep + self.city.strip() + sep + self.phone.strip() + sep + self.email.strip() + sep + self.url.strip() + sep + \
                               self.lat + sep + self.long + sep + self.lastupdated.strip()
            return (string_to_return)
        except Exception as e:
            return str(e)
    def to_csv(self,sep):
        string_to_return=self.net.strip()+sep+self.name.strip()+sep+self.street.strip()+sep+self.houseno.strip()+sep+self.postcode.strip()+sep+self.city.strip()+sep+self.phone.strip()+sep+self.email.strip()+sep+self.url.strip()+sep+str(self.lat)+sep+str(self.long)+sep+self.lastupdated.strip()
        return(string_to_return)


    def to_dict(self):
        dict_to_return=dict()
        #dict_to_return[id]=self.id
        dict_to_return['net']=self.net.strip()
        dict_to_return['name']=self.name.strip()
        dict_to_return['street']=self.street.strip()
        dict_to_return['houseno']=self.houseno.strip()
        dict_to_return['postcode']=self.postcode.strip()
        dict_to_return['city']=self.city.strip()
        dict_to_return['phone']=self.phone.strip()
        dict_to_return['email']=self.email.strip()
        dict_to_return['url']=self.url.strip()
        dict_to_return['location']=geo_json("Point",lat=self.lat,long=self.long).to_dict()
        dict_to_return['lastupdated']=self.lastupdated.strip()
        return(dict_to_return)