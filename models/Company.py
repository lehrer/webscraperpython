import uuid


class Company:
    def __init__(self, vat, name, address, phone,url, lastupdated):
        self.id = str(uuid.uuid4())
        self.vat=vat
        self.name=name
        self.address = address
        self.phone = phone
        self.url=url
        self.lastupdated=lastupdated

    def __str__(self):
        return 'vat: '+str(self.vat) + ', url: '+ str(self.url)+ ', name: '+ str(self.name) + ', phone: '+ str(self.phone)+', address: '+self.address