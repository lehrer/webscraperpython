from enum import Enum

class ContactyType(Enum):
    WEB = 'WEB'
    EMAIL = 'EMAIL'
    TEL = 'TEL'

    def __str__(self):
        return '%s' % self._value_