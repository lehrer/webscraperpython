import csv
import datetime
import json
import logging
import os
import threading
import time
from pathlib import Path
from queue import Queue

import bs4
import requests
import validators

from models.Company import Company

print_lock = threading.Lock()

# constants:
VAT_COLUMN_INDEX=1
CSV_DELIMITER_CHAR=','
INPUT_CSV_FILE = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\ReceivedRawLists\\annual_accounts_2015_2016.csv'
#BASE_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\Get BsearchInfo\\'
TIMESTAMP = datetime.datetime.now().isoformat().replace(':', '_')
OUTPUT_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\Companies\\annual_accounts_2015_2016\\companies_full\\'
LOGS_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\logs\\'
HEADERS = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
}
BSEARCH_BASE_URL = 'https://www.bsearch.be/'
THREADS = 32

# logging (based on https://stackoverflow.com/a/15167862/7194726)
if not os.path.exists(LOGS_DIR):
    os.makedirs(LOGS_DIR)
logging.getLogger('').handlers = []
logging.basicConfig(
    filename=LOGS_DIR + 'scraper_' + TIMESTAMP + '.log',
    format='%(asctime)s %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
    filemode="w",
    level=logging.DEBUG)


def isok(mypath):
    try:
        # thepage = urllib.request.urlopen(mypath)
        if mypath.endswith('.pdf'):
            return False
        if validators.url(mypath):
            return True
        else:
            return False
    except:
        # print(mypath , ' : error')
        return False


def scrape_vat(current_vat):
    vat_url = BSEARCH_BASE_URL + current_vat
    name = ''
    address = ''
    phone = ''
    url = ''
    if isok(vat_url):
        with print_lock:
            logging.info(
                'Thread: ' + str(threading.get_ident()) + ' | start grab ' + current_vat)
            print(
                'Thread: ' + str(threading.get_ident()) + ' | start grab ' + current_vat)

        try:
            response = requests.get(vat_url, headers=HEADERS)
            soup = bs4.BeautifulSoup(response.text, "html.parser")
            for n in soup.select('.name'):
                name = n.text
            for a in soup.select('.address'):
                address = a.text
            for p in soup.select('.read-more-wrap'):
                phone = p.text
            for u in soup.select('.web a'):
                url = u.attrs['href']

        except TimeoutError:
            return scrape_vat(current_vat)
        except:
            pass

        with print_lock:
            logging.info('Thread: ' + str(threading.get_ident()) + ' | end grab ' + current_vat)
            print('Thread: ' + str(threading.get_ident()) + ' | end grab ' + current_vat)
        scraped_company=Company(current_vat, name, address, phone, url, str(datetime.datetime.now()))
        logging.info('Thread: {} | scraped_company = {}'.format(str(threading.get_ident()),scraped_company.__str__()))
        print('Thread: {} | scraped_company = {}'.format(str(threading.get_ident()),scraped_company.__str__()))
        if not os.path.exists(OUTPUT_DIR):
            os.makedirs(OUTPUT_DIR)
        f_result_nl = open(OUTPUT_DIR + str(current_vat) + str('.json'), 'w',
                           encoding="utf-8")  # https://stackoverflow.com/a/42495690
        f_result_nl.write(json.dumps(scraped_company.__dict__))  # based on https://stackoverflow.com/a/10252138/7194726
        #f_result_nl.write(jsonpickle.encode(scraped_company)) #based on https://stackoverflow.com/a/10252138/7194726
        f_result_nl.close()





company_queue = Queue()
num_records_in_file = sum(1 for line in open(INPUT_CSV_FILE)) - 1

def process_queue():
    # count = 0
    while True:
        # count += 1
        current_vat = company_queue.get()
        scrape_vat(current_vat)
        company_queue.task_done()


def file_exists(path):
    my_file = Path(path)
    if my_file.is_file():
        return True
    else:
        return False


# file exists

logging.info('starting scrape with ' + str(THREADS) + ' threads')

for i in range(THREADS):
    t = threading.Thread(target=process_queue)
    t.daemon = True
    t.start()

start = time.time()

current_row=0
with open(INPUT_CSV_FILE, 'r') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=CSV_DELIMITER_CHAR)
    for row in csvreader:
        current_row += 1
        if current_row ==1:
            continue #the first row is the row with headers
        # reference = row[0]
        vat = row[VAT_COLUMN_INDEX]
        # schemaType = row[2]
        # email = row[3]
        # website = row[4]
        if file_exists(OUTPUT_DIR + str(vat) + str('.json')):
            logging.info('Thread: {} | skipping since it exists already: (vat: {} which is item no {} from a total of {}'.format(threading.get_ident(),vat, current_row,num_records_in_file))
            print('Thread: {} | skipping since it exists already: (vat: {} which is item no {} from a total of {}'.format(threading.get_ident(),vat, current_row,num_records_in_file))
            continue
        logging.info('Thread: {} | working on: (vat: {} which is item no {} from a total of {}'.format(threading.get_ident(),vat,current_row,num_records_in_file))
        print('Thread: {} | working on: (vat: {} which is item no {} from a total of {}'.format(threading.get_ident(),vat,current_row,num_records_in_file))
        #time.sleep(random.randint(1, 2))  # will sleep a random number of seconds
        company_queue.put(vat)

company_queue.join()

print(threading.enumerate())

print("Execution time = {0:.5f}".format(time.time() - start))
