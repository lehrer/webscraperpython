# import requests
#
# https://graph.facebook.com/search?type=place&fields=name,checkins,picture&q=cafe&center=40.7304,-73.9921&distance=1000&access_token=752848388438576|ae61230df506fd95f30ab40aa110340b
# https://graph.facebook.com/search?type=place&fields=name,checkins,location, link&q=kleinblatt&center=51.211598, 4.426117&distance=250000&access_token=752848388438576|ae61230df506fd95f30ab40aa110340b
#
#
# https://graph.facebook.com/endpoint?key=value&access_token=app_id|app_secret

# algemeen: zie voor uitleg: https://developers.facebook.com/docs/places/web/search
import threading
from difflib import SequenceMatcher

import os

import datetime
import requests
import logging
import logging.handlers

from requests.exceptions import SSLError

BASE_URL = "https://graph.facebook.com/search?type=place"

LOGS_DIR=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\LOGS\\'

TIMESTAMP = datetime.datetime.now().isoformat().replace(':', '_')

# logging (based on https://stackoverflow.com/a/15167862/7194726)
if not os.path.exists(LOGS_DIR):
    os.makedirs(LOGS_DIR)


# Set up a specific logger with our desired output level
my_logger = logging.getLogger('MyLogger')
my_logger.setLevel(logging.DEBUG)

# Add the log message handler to the logger
handler = logging.handlers.RotatingFileHandler(
    LOGS_DIR + 'add_facebook_info_' + TIMESTAMP + '.log', maxBytes=20, backupCount=5)
formatter = logging.Formatter('%(asctime)s %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p')
handler.setFormatter(formatter)

my_logger.addHandler(handler)

# keys
# zie https://developers.facebook.com/apps/752848388438576/settings/basic/
APP_ID = '752848388438576'
APP_SECRET = 'ae61230df506fd95f30ab40aa110340b'
# centre of Belgium is supposedly 'Le Tiège' in Nil-Saint-Vincent (Walhain). (http://www.paysdevillers-tourisme.be/en/belgian-geographical-centre)
LATITUDE = '50.633639'
LONGITUDE = '4.666778'
#400 meter in vogelvlucht
DISTANCE_METERS = '10'

# fields
# source: https://developers.facebook.com/docs/places/fields
FACEBOOK_PLACE_FIELDS = 'name,location, single_line_address, phone, link, website, description, restaurant_services, matched_categories,' + \
                        'cover, checkins, engagement, hours, is_verified, overall_star_rating, parking, payment_options, picture'


#source: https://stackoverflow.com/a/17388505/7194726
def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

def log_and_print(outputtext):
    logging.info(outputtext)
    print(outputtext)

def get_f_place_as_json(logger,search_word, city):
    try:
        my_logger=logger
        url = BASE_URL + '&access_token=' + APP_ID + '|' + APP_SECRET + '&fields=' + FACEBOOK_PLACE_FIELDS.replace(' ', '')   + '&q=' + search_word #+'&center=' + lat + ',' + long +'&distance=' + DISTANCE_METERS
        log_and_print('Thread: {} | Find_Facebook_Pages.py | request url: {}'.format(threading.get_ident(), url))
        places = requests.get(url)
        places_json = places.json()

        log_and_print('Thread: {} | Find_Facebook_Pages.py | received json: {}'.format(threading.get_ident(),places_json))

        for item in places_json['data']:
            fcbk_dict=dict()
            # print(item)
            try:
                log_and_print('Thread: {} | Find_Facebook_Pages.py | similarity: city: {} vs jsoncity: {} -> '.format(threading.get_ident(),city,item['location']['city'])+str(similar(item['location']['city'],city)))
                if similar(item['location']['city'],city) > 0.75:
                    fcbk_dict['name']=item['name']
                    fcbk_dict['city'] = item['location']['city']
                    fcbk_dict['website'] = str(item['website'])
                    fcbk_dict['link'] = str(item['link'])
                    fcbk_dict['fcbk_id'] = str(item['id'])
                    return fcbk_dict
            except KeyError as e:
                log_and_print('Thread: {} | Find_Facebook_Pages.py | Exception on getting fields (exception details: {})'.format(threading.get_ident(),str(e)))
                continue
        return None
    except (KeyError, SSLError) as e:
        log_and_print('Thread: {} | Find_Facebook_Pages.py | Exception on getting fields (exception details: {}) | search_word {}, city {}'.format(threading.get_ident(),str(e),search_word, city))
        raise e

#print(get_f_place_as_json(my_logger,'NORTHER','Oostende'))
