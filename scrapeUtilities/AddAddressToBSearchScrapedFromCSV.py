#constants
import csv
import os
import threading

from os.path import isfile, join
import json
from pathlib import Path



import datetime

from models.Company_with_extended_address import Company_with_extended_address

# COMPANIES_JSON_DIR_OLD= r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\testbedrijven\\'
# COMPANIES_JSON_DIR_NEW= COMPANIES_JSON_DIR_OLD + 'updated\\'
# INPUT_CSV_FILE=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\ReceivedRawLists\\enriched_names2_edited.csv'
# CSV_DELIMITER_CHAR=';'
# VAT_COLUMN_INDEX=4
# ADDRESS_COLUMN_INDEX=9
COMPANIES_JSON_DIR_OLD= r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\Companies\\annual_accounts_2015_2016\\companies\\'
COMPANIES_JSON_DIR_NEW= COMPANIES_JSON_DIR_OLD + 'updated\\'
INPUT_CSV_FILE=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\ReceivedRawLists\\annual_accounts_2015_2016.csv'
CSV_DELIMITER_CHAR=','
VAT_COLUMN_INDEX=1
ADDRESS_COLUMN_INDEX=9

def get_address_from_csv_file(vat):
    current_row = 0

    with open(INPUT_CSV_FILE, 'r') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=CSV_DELIMITER_CHAR)
        for row in csvreader:
            current_row += 1
            if current_row == 1:
                continue  # the first row is the row with headers
            vat_lcl = row[VAT_COLUMN_INDEX]
            if vat_lcl==vat:
                return row[ADDRESS_COLUMN_INDEX]
    return None

def update_file(json_file):
    with open(COMPANIES_JSON_DIR_OLD + json_file, 'r') as f:
        for line in f:


            # load valid lines (should probably use rstrip)
            # if len(line) < 10: continue
            # print(collection)
            new_data = {}
            old_data = json.loads(line)
            new_data_id = old_data["id"]
            new_data_vat = old_data["vat"]
            new_data_name = old_data["name"]
            new_data_b_search_address = str(old_data["address"]).strip(' \n').replace('\r','')
            if get_address_from_csv_file(new_data_vat) is not None:
                new_data_kbo_address=get_address_from_csv_file(new_data_vat)
            else:
                print(
                    'Thread: {} | skipping since no kbo address (file: {} which is item no {} from a total of {}'.format(
                        threading.get_ident(), file, current_file_no, number_of_input_files))
                continue
            new_data_phone = old_data["phone"]
            new_data_url = old_data["url"]
            new_data_lastupdated = old_data["lastupdated"]

            updated_company = Company_with_extended_address(new_data_id, new_data_vat, new_data_name, new_data_b_search_address, new_data_kbo_address,new_data_phone, new_data_url,None,None, str(datetime.datetime.now()))

            if not os.path.exists(COMPANIES_JSON_DIR_NEW):
                os.makedirs(COMPANIES_JSON_DIR_NEW)
            #print(json_file)
            f_result_nl = open(COMPANIES_JSON_DIR_NEW+json_file, 'w',encoding="utf-8")  # https://stackoverflow.com/a/42495690
            f_result_nl.write(
                json.dumps(updated_company.__dict__))  # based on https://stackoverflow.com/a/10252138/7194726
            # f_result_nl.write(jsonpickle.encode(scraped_company)) #based on https://stackoverflow.com/a/10252138/7194726
            f_result_nl.close()

def file_exists(path):
    my_file = Path(path)
    if my_file.is_file():
        return True
    else:
        return False


number_of_input_files = len([f for f in os.listdir(COMPANIES_JSON_DIR_OLD) if isfile(join(COMPANIES_JSON_DIR_OLD, f))])
current_file_no=0
for file in [f for f in os.listdir(COMPANIES_JSON_DIR_OLD) if isfile(join(COMPANIES_JSON_DIR_OLD, f))]:
    current_file_no+=1
    if file_exists(COMPANIES_JSON_DIR_NEW + file):
        print('Thread: {} | skipping since it exists already (file: {} which is item no {} from a total of {}'.format(
            threading.get_ident(),file, current_file_no, number_of_input_files))
        continue
    with open(COMPANIES_JSON_DIR_OLD+file, encoding='utf-8', errors='ignore') as company_json_file:
        print('Thread: {} | working on: file: {} which is item no {} from a total of {}'.format(
            threading.get_ident(), file, current_file_no, number_of_input_files))
        update_file(json_file=file)


