# https://nominatim.openstreetmap.org/search?q=BOULEVARD D'AVROY 38 4000 LIeGE, belgium&format=json
# https://nominatim.openstreetmap.org/search?q=Mumbai&format=jsonv2

#constants:

# constants:
import datetime
import time
import json
import logging
import logging.handlers
import os
import threading
from os.path import isfile, join
from pathlib import Path
from queue import Queue

import requests

from models.Company_with_extended_address import Company_with_extended_address
from scrapeUtilities.Find_Facebook_Pages import get_f_place_as_json

INPUT_DIR= r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\testbedrijven\\no_latlong\\'
COMPANIES_JSON_WITH_LATLONG_DIR= r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\testbedrijven\\output\\updated\\'
OUTPUT_NO_FCBK_INFO_DIR= r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\testbedrijven\\output\\no_fcbk_info\\'
LOGS_DIR=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\LOGS\\'
JSON_FILES_WITH_ISSUES_DIR= r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\testbedrijven\\output\\with_issues\\'
# COMPANIES_JSON_NO_LATLONG_DIR= r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\Companies\\annual_accounts_2015_2016\\companies\\updated\\'
# COMPANIES_JSON_WITH_LATLONG_DIR= r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\Companies\\annual_accounts_2015_2016\\withlatlong\\'
# LOGS_DIR=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\logs\\'
# JSON_FILES_WITH_ISSUES_DIR= r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\testbedrijven\\with_issues\\'

TIMESTAMP = datetime.datetime.now().isoformat().replace(':', '_')
THREADS = 32
#https://nominatim.openstreetmap.org
OS_OUTPUT_FORMAT='json' #possible options [html|xml|json|jsonv2]
OS_NOMINATIM_BASE_URL= 'https://nominatim.openstreetmap.org/search?'+'&format='+OS_OUTPUT_FORMAT
OS_PHOTON_BASE_URL='http://photon.komoot.de/api/'

#https://locationiq.org
#Sandbox: https://locationiq.org/#demo
# Documentation: https://locationiq.org/docs
# FAQs: https://locationiq.org/#faqs
LOCATIONIQ_BASE_URL='https://us1.locationiq.org/v1/search.php'
LOCATIONIQ_TOKEN='90e6efe91e9e35'
LOCATION_API_KEY='pk.53fc0902743711fbb3f69ae614cbd2a9'

#accorsing to information found on the internet, facebook places allows 600 requests per hour
# maximum. We play safe and take less then 600:
FACEBOOK_API_MAX_CONTACTS= 600 - (THREADS + 20)

HEADERS = {
    'user-agent': 'OwnSystem_for_Business_App (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36'
}
class VariablesClass:
    def __init__(self):
        self.facebook_contacted_count=0
variables= VariablesClass()

# logging (based on https://stackoverflow.com/a/15167862/7194726)
if not os.path.exists(LOGS_DIR):
    os.makedirs(LOGS_DIR)
my_logger = logging.getLogger('MyLogger')
my_logger.setLevel(logging.DEBUG)

# Add the log message handler to the logger
handler = logging.handlers.RotatingFileHandler(
    LOGS_DIR + 'add_facebook_info_' + TIMESTAMP + '.log', maxBytes=20, backupCount=5)
formatter = logging.Formatter('%(asctime)s %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p')
handler.setFormatter(formatter)

my_logger.addHandler(handler)




def file_exists(path):
    my_file = Path(path)
    if my_file.is_file():
        return True
    else:
        return False



def get_lat_long_nominatim(address):
    query='?q='+address.encode('utf-8').decode('utf-8')
    url= OS_NOMINATIM_BASE_URL + query +'&limit=1'
    log_and_print('Thread: {} | url: {}'.format(threading.get_ident,url))
    places = requests.get(url,headers=HEADERS)
    places_json = places.json()
    #log_and_print('Thread: {} | response from Nominatim received: {}'.format(threading.get_ident,places_json))

    for item in places_json:
        #print(item)
        print('lat: ' + item['lat'])
        print('lon: ' + item['lon'])
        print('display_name: ' + item['display_name'])
        if item['lat'] is not None and item['lon'] is not None:
            return '{},{}'.format(item['lat'],item['lon'])
        else:
            return None

def get_lat_long_photon(vat, address):
    query='?q='+address
    url= OS_PHOTON_BASE_URL + query +'&limit=1'
    log_and_print('Thread: {} | url: {}'.format(threading.get_ident,url))
    places=''
    places_json=''
    try:
        places = requests.get(url)
        places_json = places.json()
    except json.decoder.JSONDecodeError as e:
        log_and_print('Thread: {} | exception occured for requests.get(url) or  places.json() -> vat: {} / url: {} : exception: {}'.format(threading.get_ident,vat, url,str(e)))
        return None
    log_and_print('Thread: {} | for URL: {} we received as json response from Photon: {}'.format(threading.get_ident(),url,places.text))


    #log_and_print('Thread: {} | json response from Photon received: {}'.format(threading.get_ident,places_json))
    try:
        long=str(places_json['features'][0].get('geometry').get('coordinates')[0])
        lat=str(places_json['features'][0].get('geometry').get('coordinates')[1])
        log_and_print('Thread: {} | found lat {} and long {} (file: {})'.format(threading.get_ident(),lat,long, places_json))
        return '{},{}'.format(lat, long)
    except (IndexError):
        log_and_print('Thread: {} | skipping since no new_data_kbo_address (file: {})'.format(threading.get_ident(), places_json))
        return None

    # for item in places_json:
    #     print('lat: ' + item[0])
    #     # print('lat: ' + item['lat'])
    #     # print('lon: ' + item['lon'])
    #     # print('display_name: ' + item['display_name'])
    #     if item[0] is not None and item[0] is not None:
    #         return '{},{}'.format(item['lat'],item['lon'])
    #     else:
    #         return None

def get_lat_long_locationiq(address):
    query=address.encode('utf-8').decode('utf-8')
    data = {
        'key': LOCATIONIQ_TOKEN,
        'q': query,
        'format': 'json'
    }
    places = requests.get(LOCATIONIQ_BASE_URL,params=data)
    places_json = places.json()
    print('places_json: '+str(places_json))

    for item in places_json:
        # print(item)
        print('lat: ' + item['lat'])
        print('lon: ' + item['lon'])
        print('display_name: ' + item['display_name'])
        if item['lat'] is not None and item['lon'] is not None:
            return '{},{}'.format(item['lat'],item['lon'])
        else:
            return None


def update_file(json_file, is_get_facebook_info:bool):
    new_data_fcbk_info = None
    with open(INPUT_DIR + json_file, 'r') as f:
        for line in f:


            # load valid lines (should probably use rstrip)
            # if len(line) < 10: continue
            # print(collection)
            new_data = {}
            old_data = json.loads(line)
            new_data_id = old_data["id"]
            new_data_vat = old_data["vat"]
            new_data_name = old_data["name"]
            new_data_b_search_address=''
            new_data_kbo_address=''
            try:
                new_data_b_search_address=old_data["b_search_address"]
            except KeyError:
                log_and_print(
                    'Thread: {} | no b_search_address for file: {})'.format(
                        threading.get_ident(), json_file))
            try:
                new_data_kbo_address=old_data["kbo_adress"]
            except KeyError:
                log_and_print(
                    'Thread: {} | no kbo_adress for file: {})'.format(
                        threading.get_ident(), json_file))
            new_data_phone = old_data["phone"]
            new_data_url = old_data["url"]

            if len(new_data_kbo_address) == 0:
                updated_company = Company_with_extended_address(new_data_id, new_data_vat, new_data_name,
                                                                new_data_b_search_address, new_data_kbo_address,
                                                                new_data_phone, new_data_url, None, None,None,
                                                                str(datetime.datetime.now()))
                save_json_file(json_file, updated_company, JSON_FILES_WITH_ISSUES_DIR)
                log_and_print(
                    'Thread: {} | skipping since no new_data_kbo_address (file: {})'.format(
                        threading.get_ident(), json_file))
                continue




            #TODO: adapt accordingly
            lat_long_string=get_lat_long_photon(new_data_vat,new_data_kbo_address.encode('utf-8').decode('utf-8').replace('&',''))
            if is_get_facebook_info == True:
                variables.facebook_contacted_count+=1
                try:
                    new_data_fcbk_info=get_f_place_as_json(my_logger,new_data_name,new_data_kbo_address.split(' ')[-1])
                except:
                    new_data_fcbk_info=None
            if lat_long_string is None:
                updated_company = Company_with_extended_address(new_data_id, new_data_vat, new_data_name,
                                                                new_data_b_search_address, new_data_kbo_address,
                                                                new_data_phone, new_data_url, None, None,new_data_fcbk_info,
                                                                str(datetime.datetime.now()))
                save_json_file(json_file, updated_company, JSON_FILES_WITH_ISSUES_DIR)
                log_and_print('Thread: {} | skipping since no lat long available (file: {})'.format(threading.get_ident(), json_file))
                continue
            lat=lat_long_string.split(',')[0]
            long=lat_long_string.split(',')[1]

            updated_company = Company_with_extended_address(new_data_id, new_data_vat, new_data_name, new_data_b_search_address, new_data_kbo_address,new_data_phone, new_data_url,lat,long, new_data_fcbk_info,str(datetime.datetime.now()))

            save_json_file(json_file, updated_company, COMPANIES_JSON_WITH_LATLONG_DIR)


def save_json_file(json_file, updated_company, outputdir):
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)
    # print(json_file)
    f_result_nl = open(outputdir + json_file, 'w',
                       encoding="utf-8")  # https://stackoverflow.com/a/42495690
    f_result_nl.write(
        json.dumps(updated_company.__dict__))  # based on https://stackoverflow.com/a/10252138/7194726
    # f_result_nl.write(jsonpickle.encode(scraped_company)) #based on https://stackoverflow.com/a/10252138/7194726
    f_result_nl.close()


number_of_input_files = len([f for f in os.listdir(INPUT_DIR) if isfile(join(INPUT_DIR, f))])
current_file_no=0


def log_and_print(outputtext):
    logging.info(outputtext)
    print(outputtext)




def process_queue():
    counter = 0
    start_time = time.time()
    while True:
        #counter += 1
        # if counter % 60 == 0:
        #     time.sleep(5) # will sleep a number of seconds
        current_file = files_queue.get()
        log_and_print('Thread: {} | start on file {}'.format(threading.get_ident(),current_file))
        current_time = time.time()
        if current_time-start_time >= (3600-100): #1. calculation outputs seconds 2.we take a marge (100scds)
            if FACEBOOK_API_MAX_CONTACTS == variables.facebook_contacted_count:
                log_and_print('Thread: {} | we pause an hour because max facebook requests reached'.format(threading.get_ident()))
                time.sleep(3600)  # will sleep a number of seconds
                log_and_print('Thread: {} | let\'s continue now'.format(threading.get_ident()))
                variables.facebook_contacted_count=0

        update_file(json_file=current_file, is_get_facebook_info=True)
        files_queue.task_done()


files_queue = Queue()

for i in range(THREADS):
    t = threading.Thread(target=process_queue)
    t.daemon = True
    t.start()

start = time.time()

# for current_url in myList:
#     url_queue.put(current_url)


for file in [f for f in os.listdir(INPUT_DIR) if isfile(join(INPUT_DIR, f))]:
    current_file_no+=1
    if file_exists(COMPANIES_JSON_WITH_LATLONG_DIR + file):
        log_and_print('Thread: {} | EXISTS ALREADY --> NOT adding to queue --> file: {} which is item no {} from a total of {}'.format(
            threading.get_ident(),file, current_file_no, number_of_input_files))

        continue
    with open(INPUT_DIR + file, encoding='utf-8', errors='ignore') as company_json_file:
        # log_and_print('Thread: {} | adding to queue --> file: {} which is item no {} from a total of {}'.format(
        #     threading.get_ident(), file, current_file_no, number_of_input_files))

        files_queue.put(file)

files_queue.join()

print(threading.enumerate())

print("Execution time = {0:.5f}".format(time.time() - start))