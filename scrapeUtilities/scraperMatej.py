import datetime
import json
import logging
import os
import random
import threading
import time
import urllib
import urllib.parse
from pathlib import Path
from queue import Queue
from urllib.parse import urlsplit

import bs4
import requests
import validators
from os.path import isfile, join

from models.ScrapedUrl import ScrapedUrl

print_lock = threading.Lock()

# constants:
SCRAPE_LEVELS = 2
# BASE_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\Get BsearchInfo\\output\\'
TIMESTAMP = datetime.datetime.now().isoformat().replace(':', '_')
INPUT_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\Companies\\microsoft\\'
OUTPUT_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\scrapedUrls\\microsoft\\'
LOGS_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\logs\\'
HEADERS = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
}
THREADS = 32

# logging (based on https://stackoverflow.com/a/15167862/7194726)
if not os.path.exists(LOGS_DIR):
    os.makedirs(LOGS_DIR)
logging.getLogger('').handlers = []
logging.basicConfig(
    filename=LOGS_DIR + 'scraper_' + TIMESTAMP + '.log',
    format='%(asctime)s %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
    filemode="w",
    level=logging.DEBUG)

# variables
already_scraped_list = []  # keep track of what was already scraped
# myList = []  # list with URL's to scan
number_of_input_files = len([f for f in os.listdir(INPUT_DIR) if isfile(join(INPUT_DIR, f))])

# read the file with URL's
# f_url = open(BASE_DIR + 'be_searchlist.txt', 'r')
# for line in f_url:
#     myList.append(line.strip('\n'))
# print(myList)
# f_url.close()

# read keywords
# f_url2 = open('/Users/matejkarpisek/Documents/ISV search/ms/wordliste.txt', 'r')
myList2 = []  # list with URL's to scan


# for line2 in f_url2:
#     myList2.append(line2.strip('\n'))
# # print(myList)
# f_url2.close()


def isok(mypath):
    try:
        # thepage = urllib.request.urlopen(mypath)
        if mypath.endswith('.pdf'):
            return False
        if validators.url(mypath):
            return True
        else:
            return False
    except:
        # print(mypath , ' : error')
        return False


def file_exists(path):
    my_file = Path(path)
    if my_file.is_file():
        return True
    else:
        return False


def process_queue():
    while True:
        current_url = url_queue.get()
        if isok(current_url):
            logging.info('Thread: ' + str(threading.get_ident()) + ' | start grab ' + current_url)
            print('Thread: ' + str(threading.get_ident()) + ' | start grab ' + current_url)
            scrape_full_website(current_url, SCRAPE_LEVELS)
        url_queue.task_done()


url_queue = Queue()


def cleanMe(response):
    soup = bs4.BeautifulSoup(response.text, "html.parser")  # create a new bs4 object from the html data loaded
    for script in soup(["script", "style"]):  # remove all javascript and stylesheet code
        script.extract()
    [s.extract() for s in soup('script')]
    # get text
    text = soup.get_text(strip=False)
    # break into lines and remove leading and trailing space on each
    lines = (line.strip() + os.linesep for line in text.splitlines())
    # break multi-headlines into a line each
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
    # drop blank lines
    text = ' '.join(chunk for chunk in chunks if chunk)
    return text


def scrape_specific_url(url_link, levels):
    if levels == 0:
        return ''
    scraped_text = []

    if isok(url_link):
        with print_lock:
            number_of_processed = len([f for f in os.listdir(OUTPUT_DIR) if isfile(join(OUTPUT_DIR, f))])
            logging.info(
                'Thread {} | start capturing {} | level {} | No. {} from {}'.format(threading.get_ident(),
                                                                                       url_link, str(
                        SCRAPE_LEVELS + 1 - levels), number_of_processed - 1, number_of_input_files))
            print('Thread {} | start capturing {} | level {} | No. {} from {}'.format(threading.get_ident(),
                                                                                         url_link, str(
                    SCRAPE_LEVELS + 1 - levels), number_of_processed - 1, number_of_input_files))

        try:
            response = requests.get(url_link, headers=HEADERS)
            soup = bs4.BeautifulSoup(response.text, "html.parser")
            scraped_text.append(cleanMe(response))
            already_scraped_list.append(url_link)
            for link in soup.find_all('a'):
                # time.sleep(random.randint(1, 2))  # will sleep a random number of seconds
                internal_url_link = str(link.get('href'))
                if internal_url_link.startswith('/') and url_link.endswith('/'):
                    internal_url_link = url_link + internal_url_link.replace('/', '', 1)
                if internal_url_link.startswith('#'):
                    internal_url_link = url_link + internal_url_link
                if isok(internal_url_link):
                    try:
                        # response2 = requests.get(internal_url_link, headers=HEADERS)
                        # scraped_text.append(cleanMe(response2))
                        if (internal_url_link not in already_scraped_list):
                            scraped_text.append('\n' + scrape_specific_url(internal_url_link, levels - 1))
                    except TimeoutError:
                        scraped_text.append('\n' + scrape_specific_url(internal_url_link, levels - 1))
                    except:
                        pass
        except:
            pass

        if isok(url_link):
            with print_lock:
                number_of_processed = len([f for f in os.listdir(OUTPUT_DIR) if isfile(join(OUTPUT_DIR, f))])
                logging.info(
                    'Thread {} | finished capturing {} | level {} | No. {} from {}'.format(threading.get_ident(),
                                                                                           url_link, str(
                            SCRAPE_LEVELS + 1 - levels), number_of_processed - 1, number_of_input_files))
                print('Thread {} | finished capturing {} | level {} | No. {} from {}'.format(threading.get_ident(),
                                                                                             url_link, str(
                        SCRAPE_LEVELS + 1 - levels), number_of_processed - 1, number_of_input_files))

        return scraped_text

        # for scan_keyword in myList2:
        #     f_result_nl.write(str(str.lower(viz_text).count(scan_keyword)) + ';')
        #     # print(url_link)


def scrape_full_website(current_url, levels):
    url_parsed = urllib.parse.urlparse(current_url)[1]
    filename = OUTPUT_DIR + str(url_parsed) + str('.json')
    scraped_text = scrape_specific_url(current_url, levels)
    scrapedUrlObject = ScrapedUrl(current_url, url_parsed, scraped_text)
    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)
    f_result_nl = open(filename, 'w', encoding="utf-8")  # https://stackoverflow.com/a/42495690
    # f_result_nl.write(current_url + '\n' + str(scraped_text))
    f_result_nl.write(json.dumps(scrapedUrlObject.__dict__))  # based on https://stackoverflow.com/a/10252138/7194726
    # f_result_nl.write(jsonpickle.encode(scrapedUrlObject)) #based on https://stackoverflow.com/a/10252138/7194726
    f_result_nl.close()
    number_of_processed = len([f for f in os.listdir(OUTPUT_DIR) if isfile(join(OUTPUT_DIR, f))])
    print('Thread {} | Captured {} | Already processed {} from {}'.format(threading.get_ident(), url_parsed,
                                                                         number_of_processed, number_of_input_files))
    logging.info('Thread {} | Captured {} | Already processed {} from {}'.format(threading.get_ident(), url_parsed,
                                                                                 number_of_processed,
                                                                                 number_of_input_files))


logging.info('starting scrape with ' + str(THREADS) + ' threads and ' + str(SCRAPE_LEVELS) + ' depth levels')
print('input_dir: ' + INPUT_DIR)

for i in range(THREADS):
    t = threading.Thread(target=process_queue)
    t.daemon = True
    t.start()

start = time.time()

# for current_url in myList:
#     url_queue.put(current_url)

for file in [f for f in os.listdir(INPUT_DIR) if isfile(join(INPUT_DIR, f))]:
    with open(INPUT_DIR + file, encoding='utf-8', errors='ignore') as fh:
        data = json.load(fh)
        current_url = data['url']
        if len(current_url) > 0:
            url_parsed = (urllib.parse.urlparse(current_url)[1]).strip().replace('\\', '_')
            # print(url_parsed)
            filename = OUTPUT_DIR + str(url_parsed) + str('.json')
            if file_exists(filename):
                print('Thread: {} | Skipping since exists already {}'.format(threading.get_ident(), url_parsed))
                logging.info('Thread: {} | Skipping since exists already {}'.format(threading.get_ident(), url_parsed))
                continue
            url_queue.put(current_url)

url_queue.join()

print(threading.enumerate())

print("Execution time = {0:.5f}".format(time.time() - start))
