#based on https://pythonspot.com/flask-web-forms/
#and http://flask.pocoo.org/docs/1.0/quickstart/#a-minimal-application
import os

from flask import Flask, render_template, flash, request, redirect, url_for, send_from_directory
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField
from werkzeug.utils import secure_filename
from flask_socketio import SocketIO, send, emit

# App config.
DEBUG = True
UPLOAD_FOLDER=''
ALLOWED_EXTENSIONS = {'txt', 'csv'}
app = Flask(__name__)
app.config.from_object(__name__)
# Set the secret key to some random bytes. Keep this really secret!
#create new one $ python -c 'import os; print(os.urandom(16))'
app.secret_key = b'\xcb;\x13\xfdN\xdf\xb2|j\x11\x9d\xaeH\xf1\xa1\x85'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 100 * 1024 * 1024 #100MB
socketio = SocketIO(app)



class ReusableForm(Form):
    name = StringField('Name:', validators=[validators.required()])
    email = StringField('Email:', validators=[validators.required(), validators.Length(min=6, max=35)])
    password = StringField('Password:', validators=[validators.required(), validators.Length(min=3, max=35)])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route("/", methods=['GET', 'POST'])
def hello():
    form = ReusableForm(request.form)


    print(form.errors)
    if request.method == 'POST':
        name = request.form['name']
        password = request.form['password']
        email = request.form['email']
        print(name, " ", email, " ", password)

        if form.validate():
            # Save the comment here.
            flash('Thanks for registration ' + name)
        else:
            flash('Error: All the form fields are required. ')

    return render_template('MainFormBeta.html', form=form)


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('uploaded_file',
                                    filename=filename))
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''
@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)

@socketio.on('message')
def handleMessage(msg):
    print('Message: ' + msg)
    send(msg, broadcast=True)

@socketio.on('my event')
def test_message(message):
    emit('my response', {'data': message['data']})


if __name__ == "__main__":
    #app.run()
    socketio.run(app)