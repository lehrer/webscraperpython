#based on https://pythonspot.com/flask-web-forms/
#and http://flask.pocoo.org/docs/1.0/quickstart/#a-minimal-application
import datetime
import os
import threading
from threading import Lock
from flask import Flask, render_template, flash, request, redirect, url_for, send_from_directory
#from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField, FileField
from flask_wtf import FlaskForm
from os.path import isfile, join
from wtforms import Form, BooleanField, StringField, PasswordField, validators, SubmitField, IntegerField
from flask_wtf.file import FileField, FileRequired
from werkzeug.utils import secure_filename
from flask_socketio import SocketIO, send, emit

# App config.
from wtforms.validators import DataRequired
from wtforms.widgets.html5 import NumberInput

from UI.Flask.CleanCache import nocache
from utilities.DTM_Creator import DTM_Creator
from utilities.Scraper import Scraper
from companies_model_editor.Update_companies_in_mongoDB import Update_companies_in_DB
from utilities.utilities import get_column_value_from_csv_file, read_config_file_return_configs_list
import eventlet

MONDOGDB_SERVER = 'localhost'
MONGODB_PORT = 27017
MONGODB_DB = 'companies_db'
#some collection names:
COLLECTION_FULL_ADDRESSES="full_addresses"
COLLECTION_LATLONG_COORDS="latlong_coordinates"
COLLECTION_URLS="urls"
COLLECTION_RECORDED_EMPLOYEES="recorded_employees"
COLLECTION_SCRAPED_CONTENT="scraped_content"

SUBMIT_BTN_DTM='Submit for Dtm'
SUBMIT_BTN_SCRAPE='Start Scraping URL from the DB'
SUBMIT_BTN_LATLNG_EXPORT_CSV='Submit for latlong export'
SUBMIT_BTN_ADD_NEW_LTLNG_TO_DB='Start adding new LatLong to the DB'

DEBUG = True
CONFIG_FILE='config.txt'
CONFIG_DICT=read_config_file_return_configs_list(os.path.dirname(os.path.realpath(__file__))+'\\'+CONFIG_FILE)
#BASEDIR= r'%s'%''.join(CONFIG_DICT['base_dir'])
#UPLOAD_FOLDER=r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Easy_Order\\flask_test\\'
#OUTPUT_FOLDER=UPLOAD_FOLDER+'generated\\'

UPLOAD_FOLDER= r'%s'%CONFIG_DICT['base_dir']
OUTPUT_FOLDER= r'%s'%CONFIG_DICT['output_dir']
ALLOWED_EXTENSIONS = {'txt', 'csv'}

eventlet.monkey_patch()
app = Flask(__name__)
app.config.from_object(__name__)
# Set the secret key to some random bytes. Keep this really secret!
#create new one $ python -c 'import os; print(os.urandom(16))'
app.secret_key = b'\xcb;\x13\xfdN\xdf\xb2|j\x11\x9d\xaeH\xf1\xa1\x85'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['OUTPUT_FOLDER'] = OUTPUT_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 100 * 1024 * 1024 #100MB

# Set this variable to "threading", "eventlet" or "gevent" to test the
# different async modes, or leave it set to None for the application to choose
# the best option based on installed packages.
async_mode = None

socketio = SocketIO(app, async_mode=async_mode,ping_timeout  =600)
thread = None
thread_lock = Lock()



class DtmForm(FlaskForm):
    fileselector = FileField('Select csv file with keywords',validators=[FileRequired()])
    dtm_column_field=IntegerField(u'Which column contains the value',widget=NumberInput(), validators=[validators.optional()],default='0')
    seperator=StringField(u'Enter seperator')
    submit= SubmitField(SUBMIT_BTN_DTM,validators=[DataRequired()])

class LatLongExportCsvForm(FlaskForm):
    fileselector = FileField('Select csv file with vat to include in latlong',validators=[FileRequired()])
    vat_column_field=IntegerField(u'Which column contains the vat',widget=NumberInput(), validators=[validators.optional()],default='0')
    seperator=StringField(u'Enter seperator')
    submit= SubmitField(SUBMIT_BTN_LATLNG_EXPORT_CSV,validators=[DataRequired()])

class AddNewLatLongToDbForm(FlaskForm):
    fileselector = FileField('Select csv file with vat to check if not in DB yet', validators=[FileRequired()])
    vat_column_field = IntegerField(u'Which column contains the vat', widget=NumberInput(),
                                    validators=[validators.optional()], default='0')
    seperator = StringField(u'Enter seperator')
    submit = SubmitField(SUBMIT_BTN_ADD_NEW_LTLNG_TO_DB,validators=[DataRequired()])

class ScraperForm(FlaskForm):
    submit= SubmitField(SUBMIT_BTN_SCRAPE,validators=[DataRequired()])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/', methods=['POST'])
def scrape():
    lat_long_form = LatLongExportCsvForm()
    dtm_form = DtmForm()
    scraper_form = ScraperForm()
    add_new_latlong_to_db_form = AddNewLatLongToDbForm()
    for itm in request.form:
        print(itm)
    print(request.form['submit'])
    if request.method == 'POST' and request.form['submit']!=SUBMIT_BTN_SCRAPE:
        add_latlong_to_db_if_not_existing()
    elif request.method=='POST':
        print('scrape()')
        print(lat_long_form.submit.data)
        print(scraper_form.submit.data)
        scraper = Scraper(app,MONDOGDB_SERVER,MONGODB_PORT,MONGODB_DB)
        t1 = threading.Thread(target=scraper.scrape_with_db_if_url_field_exists(COLLECTION_URLS,COLLECTION_SCRAPED_CONTENT))
        t1.setDaemon(True)
        t1.start()

        emit('message', 'scraping finished', namespace='/',broadcast=True)
        return redirect(request.url)

    return render_template('MainForm.html', add_new_latlong_to_db_form=add_new_latlong_to_db_form,
                           lat_long_form=lat_long_form, scraper_form=scraper_form, dtm_form=dtm_form,
                           async_mode=socketio.async_mode)

@app.route('/', methods=['POST'])
def add_latlong_to_db_if_not_existing():
    lat_long_form = LatLongExportCsvForm()
    dtm_form = DtmForm()
    scraper_form = ScraperForm()
    add_new_latlong_to_db_form = AddNewLatLongToDbForm()
    if request.method == 'POST' and request.form['submit']!=SUBMIT_BTN_ADD_NEW_LTLNG_TO_DB:
        get_lat_longs()
    elif request.method == 'POST':
        print('latlong!!')
       # check if the post request has the file part
        if 'fileselector' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['fileselector']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(
                '{}_{}'.format(datetime.datetime.now().isoformat().replace(':', '_'), file.filename))
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            seperator = None
            if len(request.form['seperator']) > 0:
                seperator = request.form['seperator']
            t1 = threading.Thread(target=add_non_existing_vats_to_db(os.path.join(app.config['UPLOAD_FOLDER'], filename),request.form['vat_column_field'],seperator))

            t1.setDaemon(True)
            t1.start()
        return redirect(request.url)
    print(lat_long_form.errors)
    return render_template('MainForm.html', add_new_latlong_to_db_form=add_new_latlong_to_db_form,
                           lat_long_form=lat_long_form, scraper_form=scraper_form, dtm_form=dtm_form,
                           async_mode=socketio.async_mode)

@app.route('/', methods=['POST'])
def get_lat_longs():
    lat_long_form = LatLongExportCsvForm()
    dtm_form = DtmForm()
    scraper_form = ScraperForm()
    add_new_latlong_to_db_form = AddNewLatLongToDbForm()
    if request.method == 'POST' and request.form['submit']!=SUBMIT_BTN_LATLNG_EXPORT_CSV:
        dtm()
    elif request.method=='POST':
        print('get_lat_longs()')

       # check if the post request has the file part
        if 'fileselector' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['fileselector']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(
                '{}_{}'.format(datetime.datetime.now().isoformat().replace(':', '_'), file.filename))
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            seperator = None
            if len(request.form['seperator']) > 0:
                seperator = request.form['seperator']

            t1 = threading.Thread(target=create_lat_longcsv(os.path.join(app.config['UPLOAD_FOLDER'], filename),request.form['vat_column_field'],seperator))

            t1.setDaemon(True)
            t1.start()
        return redirect(request.url)
    return render_template('MainForm.html', add_new_latlong_to_db_form=add_new_latlong_to_db_form,
                           lat_long_form=lat_long_form, scraper_form=scraper_form, dtm_form=dtm_form,
                           async_mode=socketio.async_mode)



@app.route('/', methods=['GET', 'POST'])
def dtm():
    lat_long_form = LatLongExportCsvForm()
    dtm_form = DtmForm()
    scraper_form = ScraperForm()
    add_new_latlong_to_db_form = AddNewLatLongToDbForm()
    if request.method == 'POST' and request.form['submit']==SUBMIT_BTN_DTM:
        # check if the post request has the file part
        if 'fileselector' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['fileselector']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename('{}_{}'.format(datetime.datetime.now().isoformat().replace(':', '_'),file.filename))
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            # return redirect(url_for('uploaded_file', filename=filename))
            # for f in request.form:
            #     print(f)
            seperator=None
            if len(request.form['seperator'])>0:
                seperator=request.form['seperator']
            t1 = threading.Thread(target=create_dtm(os.path.join(app.config['UPLOAD_FOLDER'], filename),request.form['dtm_column_field'],seperator))
            t1.setDaemon(True)
            t1.start()
            # threads = []
            # process = threading.Thread(target=create_dtm, args=[os.path.join(app.config['UPLOAD_FOLDER'], filename),request.form['dtm_column_field'],seperator])
            # process.start()
            # threads.append(process)
            return redirect(request.url)

    return render_template('MainForm.html', add_new_latlong_to_db_form=add_new_latlong_to_db_form,
                           lat_long_form=lat_long_form, scraper_form=scraper_form, dtm_form=dtm_form,
                           async_mode=socketio.async_mode)


# def init_forms():
#     dict={}
#     dict['lat_long_form'] = LatLongExportCsvForm()
#     dict['dtm_form'] = DtmForm()
#     dict['scraper_form'] = ScraperForm()
#     dict['add_new_latlong_to_db_form'] = AddNewLatLongToDbForm()
#
#     return dict


# def render_mainform_template(dict):
#     lat_long_form = dict['lat_long_form']
#     dtm_form =  dict['dtm_form']
#     scraper_form = dict['scraper_form']
#     add_new_latlong_to_db_form= dict['add_new_latlong_to_db_form']
#
#     return render_template('MainForm.html', add_new_latlong_to_db_form=add_new_latlong_to_db_form,lat_long_form=lat_long_form, scraper_form=scraper_form, dtm_form=dtm_form,
#                            async_mode=socketio.async_mode)


def create_dtm(keywords_file,columnid:int,seperator:str):
    keywords_list=get_column_value_from_csv_file(keywords_file,columnid,seperator)
    emit('message', 'we have the following list \n{}'.format(keywords_list), namespace='/',broadcast=True)
    dtm_creator = DTM_Creator(app,MONDOGDB_SERVER, MONGODB_PORT, MONGODB_DB)

    # # with scraped content of all urls
    dtm_file_name='{}_dtm.csv'.format(datetime.datetime.now().isoformat().replace(':', '_'))
    complete_filepath=os.path.join(app.config['OUTPUT_FOLDER'], dtm_file_name)
    dtm_creator.create(complete_filepath, COLLECTION_SCRAPED_CONTENT, keywords_list, None)
    emit('message', 'Download the file <a href="{0}">{0}</a>'.format(complete_filepath), namespace='/',broadcast=True)
    emit('file_urls_message', '<a href="{0}">{0}</a>'.format(complete_filepath), namespace='/', broadcast=True)

def create_lat_longcsv(vats_file,columnid:int,seperator:str):
    vat_list=get_column_value_from_csv_file(vats_file,columnid,seperator)
    print(type(vat_list))
    emit('message', 'we have the following list \n{}'.format(vat_list), namespace='/', broadcast=True)
    cmpny_updtr = Update_companies_in_DB(MONDOGDB_SERVER, MONGODB_PORT, MONGODB_DB)
    latlong_file_name='{}_latlong.csv'.format(datetime.datetime.now().isoformat().replace(':', '_'))
    complete_filepath=os.path.join(app.config['OUTPUT_FOLDER'], latlong_file_name)
    vat_array = ['BE0207559115', 'BE0400123812', 'BE0400369676']
    for itm in vat_list:
        print(itm)
    for itm in vat_array:
        print(itm)
    cmpny_updtr.generate_csv_list_with_vat_and_latlong_and_url(app,latlong_collection=COLLECTION_LATLONG_COORDS,url_collection=COLLECTION_URLS,output_csv_file=complete_filepath,vat_array=vat_list)

    emit('message', 'Download the file <a href="{0}">{0}</a>'.format(complete_filepath), namespace='/', broadcast=True)
    emit('file_urls_message', '<a href="{0}">{0}</a>'.format(complete_filepath), namespace='/', broadcast=True)

def add_non_existing_vats_to_db(vats_file,columnid:int,seperator:str):
    vat_list = get_column_value_from_csv_file(vats_file, columnid, seperator)
    emit('message', 'we have the following list \n{}'.format(vat_list), namespace='/', broadcast=True)
    cmpny_updtr = Update_companies_in_DB(MONDOGDB_SERVER, MONGODB_PORT, MONGODB_DB)
    cmpny_updtr.collect_btws_from_given_list_and_add_coords_to_db(app=app, vat_array=vat_list,vat_column_id=columnid,db_full_address_collection=COLLECTION_FULL_ADDRESSES,db_coords_collection=COLLECTION_LATLONG_COORDS)
    emit('message', 'Done checking for non existing vats in coords db for {}'.format(vats_file), namespace='/', broadcast=True)


@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['OUTPUT_FOLDER'],
                               filename)




@socketio.on('message', namespace='/')
def handleMessage(msg):
    print('Message: ' + msg)
    send(msg, broadcast=True)

@socketio.on('file_urls_message',namespace='/')
def handleFileUrlLocationMessage(url):
    send(url, broadcast=True)

@socketio.on('collect_all_archived_files', namespace='/')
def collect_all_archived_files():
    onlyfiles = [f for f in os.listdir(app.config['OUTPUT_FOLDER']) if isfile(os.path.join(app.config['OUTPUT_FOLDER'], f))]
    for file in onlyfiles:
        emit('file_urls_message', '<a href="{0}">{0}</a>'.format(os.path.join(app.config['OUTPUT_FOLDER'],file)), namespace='/',broadcast=True)



if __name__ == "__main__":
    #app.run()
    socketio.run(app)