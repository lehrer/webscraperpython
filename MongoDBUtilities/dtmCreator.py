import csv
import datetime
import json
import re
from collections import defaultdict
from itertools import chain

import os
from os.path import sep

import logging
from pymongo import MongoClient



# constants
BASE_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\Microsoft\\Get BsearchInfo\\toBeScraped\\done\\' \
           + '\\'
TIMESTAMP = datetime.datetime.now().isoformat().replace(':', '_')
OUTPUT_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\dtm\\'
LOGS_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\logs\\'
MONDOGDB_SERVER = 'localhost'
MONGODB_PORT = 27017
MONGODB_DB = 'scraped_urls_db'
COLLECTION_NAME="collection"

KEYWORDS_ARRAY = ['cookies', 'data']

# logging (based on https://stackoverflow.com/a/15167862/7194726)
if not os.path.exists(LOGS_DIR):
    os.makedirs(LOGS_DIR)
logging.getLogger('').handlers = []
logging.basicConfig(
    filename=LOGS_DIR+'dtm_' + TIMESTAMP + '.log',
    format= '%(asctime)s %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
    filemode="w",
    level=logging.DEBUG)

def find_specific(keyword):
    client = MongoClient(MONDOGDB_SERVER, MONGODB_PORT)
    db = client[MONGODB_DB]
    db_collection_name=COLLECTION_NAME
    dict = {}
    regx = re.compile(keyword, re.IGNORECASE)
    for document in db[db_collection_name].find({'scrapedContent': regx}):
        number_of_occurences = len(re.findall(regx, document["scrapedContent"]))
        logging.info('found the word "'+keyword+'": '+str(number_of_occurences) +' time(s) in ' + str(document["url_not_parsed"]))
        dict[document["url_not_parsed"]] = (str(number_of_occurences))
    logging.info('#found results for keyword: {}: {}'.format(keyword,str(db[db_collection_name].find({'scrapedContent': regx}).count())))
    print('#found results for keyword: {}: {}'.format(keyword,str(db[db_collection_name].find({'scrapedContent': regx}).count())))
    return dict

def save_to_file(dir,filename, content):
    if not os.path.exists(OUTPUT_DIR):
        os.makedirs(OUTPUT_DIR)
    f_result_nl = open(OUTPUT_DIR + filename, 'w',
                       encoding="utf-8")  # https://stackoverflow.com/a/42495690
    f_result_nl.write(content) #based on https://stackoverflow.com/a/10252138/7194726
    f_result_nl.close()


dtm_dict = {}
url_set = set()
for keyword in KEYWORDS_ARRAY:
    dtm_dict[keyword.lower()] = find_specific(keyword.lower())
for k in dtm_dict:
    for k1 in dtm_dict[k]:
        url_set.add(k1)  # add all urls in a set (so only unique values are in it

headers='url;'
rows_dict={}

#prepare columnheader:
for keyword in KEYWORDS_ARRAY:
    # add keyword to headers
    headers += keyword + ';'

#prepare each row
for url in url_set:
    #first init all rows (we don't want Null errors)
    if url not in rows_dict.keys():
        rows_dict[url] = ''
    for keyword in KEYWORDS_ARRAY:
        #if there is no value, give it value '0'
        if url not in dtm_dict[keyword].keys():
            dtm_dict[keyword][url] = '0'
        # prepare each row
        rows_dict[url] = rows_dict[url]+str(dtm_dict[keyword][url]) + ';'

output=headers+'\n'
for url in url_set:
    output+='{};{}\n'.format(url,rows_dict[url])

logging.info('saving file with the following output:')
logging.info(output)
print(output)
save_to_file(OUTPUT_DIR,'dtm_{}.csv'.format(TIMESTAMP),output)


