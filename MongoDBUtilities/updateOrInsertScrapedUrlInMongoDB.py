import datetime
import json
import os
import threading
from genericpath import isfile
from os.path import join

import pymongo
import logging
from pymongo import MongoClient

# constants
from pymongo.errors import DuplicateKeyError

#BASE_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\Get BsearchInfo\\output\\'
INPUT_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\scrapedUrls\\'
LOGS_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\logs\\'
TIMESTAMP = datetime.datetime.now().isoformat().replace(':', '_')
# OUTPUT_DIR = BASE_DIR + '\\done\\' + TIMESTAMP + '\\'
MONDOGDB_SERVER = 'localhost'
MONGODB_PORT = 27017
MONGODB_DB = 'scraped_urls_db'
COLLECTION_NAME="collection"

# logging (based on https://stackoverflow.com/a/15167862/7194726)
if not os.path.exists(LOGS_DIR):
    os.makedirs(LOGS_DIR)
logging.getLogger('').handlers = []
logging.basicConfig(
    filename=LOGS_DIR + 'update_scraped_url_db_' + TIMESTAMP + '.log',
    format='%(asctime)s %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
    filemode="w",
    level=logging.DEBUG)


# main functions:

##     IMPORT records into mongo


def insert_file_in_db(json_file: str, db,db_collection, upsert):
    result = db.profiles.create_index([('id', pymongo.ASCENDING)], unique=True)

    # print('Current index: ' + str(sorted(list(db.profiles.index_information()))))

    # A collection is a group of documents stored in MongoDB, and can be thought of as roughly the equivalent of
    # a table in a relational database. Getting a collection in PyMongo works the same as getting a database:
    collection = db.items

    counter = 0
    # print ('json_file: '+json_file)
    with open(json_file, 'r') as f:
        for line in f:

            # load valid lines (should probably use rstrip)
            # if len(line) < 10: continue
            try:
                # print(collection)
                new_data = {}
                old_data = json.loads(line)
                new_data["id"] = old_data["id"]
                new_data["url_not_parsed"] = old_data["url_not_parsed"]
                new_data["url_parsed"] = old_data["url_parsed"]
                new_data["scrapedContent"] = (' '.join(old_data["scrapedContent"]))
                # db[db_collection].insert(new_data)
                db[db_collection].update_one(filter={'url_parsed': new_data["url_parsed"]}, update={"$set": new_data},
                                            upsert=upsert)
            except DuplicateKeyError as dke:
                print("Duplicate Key Error: ", dke)
            except ValueError as e:
                print("Value Error: ", e)
                # friendly log message
                if 0 == counter % 100 and 0 != counter:
                    print("loaded line: ", counter)

        f.close()


number_of_input_files = len([f for f in os.listdir(INPUT_DIR) if isfile(join(INPUT_DIR, f))])
number_of_processed = 0
client = MongoClient(MONDOGDB_SERVER, MONGODB_PORT)
db = client[MONGODB_DB]
db_collection=COLLECTION_NAME
# clear first the db
#db[db_collection].delete_many({})
for file in [f for f in os.listdir(INPUT_DIR) if isfile(join(INPUT_DIR, f))]:
    number_of_processed += 1
    print('Thread {} | JsonFile: {} | Processing {} from {}'.format(threading.get_ident(), file, number_of_processed,
                                                                    number_of_input_files))
    logging.info(
        'Thread {} | JsonFile: {} | Processing {} from {}'.format(threading.get_ident(), file, number_of_processed,
                                                                  number_of_input_files))
    with open(INPUT_DIR + file, encoding='utf-8', errors='ignore') as company_json_file:
        insert_file_in_db(json_file=company_json_file.name, db=db,db_collection=db_collection, upsert=True)

    print('Thread {} | JsonFile: {} | Processed {} from {}'.format(threading.get_ident(), file, number_of_processed,
                                                                   number_of_input_files))
    logging.info(
        'Thread {} | JsonFile: {} | Processed {} from {}'.format(threading.get_ident(), file, number_of_processed,
                                                                 number_of_input_files))
