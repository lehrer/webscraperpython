import datetime
import json
import os
import threading
from genericpath import isfile
from os.path import join
import logging


import pymongo
from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError


# constants
BASE_DIR = r'C:\\Users\\alain\\OneDrive\\Documents\\GlickmanWork\\ScrapeCompanies\\output\\Companies\\annual_accounts_2015_2016\\companies\\'
INPUT_DIR = BASE_DIR
LOGS_DIR = BASE_DIR + 'logs\\'
TIMESTAMP = datetime.datetime.now().isoformat().replace(':', '_')
# OUTPUT_DIR = BASE_DIR + '\\done\\' + TIMESTAMP + '\\'
MONDOGDB_SERVER = 'localhost'
MONGODB_PORT = 27017
MONGODB_DB = 'companies_db'
COLLECTION_NAME="urls_from_bsearch"

# logging (based on https://stackoverflow.com/a/15167862/7194726)
if not os.path.exists(LOGS_DIR):
    os.makedirs(LOGS_DIR)
logging.getLogger('').handlers = []
logging.basicConfig(
    filename=LOGS_DIR + 'update_company_db_' + TIMESTAMP + '.log',
    format='%(asctime)s %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
    filemode="w",
    level=logging.DEBUG)


# main functions:

##     IMPORT records into mongo


def insert_file_in_db(json_file: str, db,db_collection, upsert):
    result = db.profiles.create_index([('id', pymongo.ASCENDING)], unique=True)

    # print('Current index: ' + str(sorted(list(db.profiles.index_information()))))

    # A collection is a group of documents stored in MongoDB, and can be thought of as roughly the equivalent of
    # a table in a relational database. Getting a collection in PyMongo works the same as getting a database:
    collection = db.items

    counter = 0
    # print ('json_file: '+json_file)
    with open(json_file, 'r') as f:
        for line in f:

            # load valid lines (should probably use rstrip)
            # if len(line) < 10: continue
            try:
                # print(collection)
                new_data = {}
                old_data = json.loads(line)
                new_data["id"] = old_data["id"]
                new_data["vat"] = old_data["vat"]
                new_data["name"] = old_data["name"]
                new_data["address"] = str(old_data["address"]).strip(' \n').replace('\r','')
                #print(repr(new_data["address"]))
                new_data["phone"] = old_data["phone"]
                new_data["url"] = old_data["url"]
                new_data["lastupdated"] = old_data["lastupdated"]

                # db[db_collection].insert(new_data)
                db[db_collection].update_one(filter={'vat': new_data["vat"]}, update={"$set": new_data},
                                            upsert=upsert)
            except DuplicateKeyError as dke:
                print("Duplicate Key Error: ", dke)
            except ValueError as e:
                print("Value Error: ", e)
                # friendly log message
                if 0 == counter % 100 and 0 != counter:
                    print("loaded line: ", counter)

        f.close()


number_of_input_files = len([f for f in os.listdir(INPUT_DIR) if isfile(join(INPUT_DIR, f))])
number_of_processed = 0
client = MongoClient(MONDOGDB_SERVER, MONGODB_PORT)
db = client[MONGODB_DB]
db_collection=COLLECTION_NAME
# clear first the db
#db[db_collection].delete_many({})
for file in [f for f in os.listdir(INPUT_DIR) if isfile(join(INPUT_DIR, f))]:
    number_of_processed += 1
    print('Thread {} | JsonFile: {} | Processing {} from {}'.format(threading.get_ident(), file, number_of_processed,
                                                                    number_of_input_files))
    logging.info(
        'Thread {} | JsonFile: {} | Processing {} from {}'.format(threading.get_ident(), file, number_of_processed,
                                                                  number_of_input_files))
    with open(INPUT_DIR + file, encoding='utf-8', errors='ignore') as company_json_file:
        insert_file_in_db(json_file=company_json_file.name, db=db,db_collection=db_collection, upsert=True)

    print('Thread {} | JsonFile: {} | Processed {} from {}'.format(threading.get_ident(), file, number_of_processed,
                                                                   number_of_input_files))
    logging.info(
        'Thread {} | JsonFile: {} | Processed {} from {}'.format(threading.get_ident(), file, number_of_processed,
                                                                 number_of_input_files))
